/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.trading.client.model;

import com.google.gwt.user.datepicker.client.CalendarUtil;

import java.util.Date;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public enum Tenor {

    SP("SP") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addDaysToDate(result, 3);
            return result;
        }
    }, TDY("TDY") {
        @Override
        public Date getValue(Date date) {
            return (Date) date.clone();
        }
    }, TOM("TOM") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addDaysToDate(result, 1);
            return result;
        }
    }, _1D("1D") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addDaysToDate(result, 2);
            return result;
        }
    }, _1W("1W") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addDaysToDate(result, 7);
            return result;
        }
    }
    , _2W("2W") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addDaysToDate(result, 14);
            return result;
        }
    }
    , _3W("3W") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addDaysToDate(result, 21);
            return result;
        }
    }
    , _1M("1M") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addMonthsToDate(result, 1);
            return result;
        }
    }
    , _2M("2M") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addMonthsToDate(result, 2);
            return result;
        }
    }
    , _3M("3M") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addMonthsToDate(result, 3);
            return result;
        }
    }
    , _4M("4M") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addMonthsToDate(result, 4);
            return result;
        }
    }
    , _5M("5M") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addMonthsToDate(result, 5);
            return result;
        }
    }
    , _6M("6M") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addMonthsToDate(result, 6);
            return result;
        }
    }
    , _7M("7M") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addMonthsToDate(result, 7);
            return result;
        }
    }
    , _8M("8M") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addMonthsToDate(result, 8);
            return result;
        }
    }
    , _9M("9M") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addMonthsToDate(result, 9);
            return result;
        }
    }
    , _10M("10M") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addMonthsToDate(result, 10);
            return result;
        }
    }
    , _1Y("1Y") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addMonthsToDate(result, 12);
            return result;
        }
    }
    , _15M("15M") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addMonthsToDate(result, 15);
            return result;
        }
    }
    , _18M("18M") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addMonthsToDate(result, 18);
            return result;
        }
    }
    , _21M("21M") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addMonthsToDate(result, 21);
            return result;
        }
    }
    , _2Y("2Y") {
        @Override
        public Date getValue(Date date) {
            final Date result = (Date) date.clone();
            CalendarUtil.addMonthsToDate(result, 24);
            return result;
        }
    };

    private final String name;

    Tenor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract Date getValue(Date now);
}
