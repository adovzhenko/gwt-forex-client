/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.trading.client.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class MatrixRowModelCellValue implements Serializable {
	
	public enum Status {
		BUY, SELL, NONE, NO_DATA;
	}
	
	private BigDecimal price;
	private Status status;

    @SuppressWarnings("unused")
    private MatrixRowModelCellValue() {}
	
	public MatrixRowModelCellValue(BigDecimal price, Status status) {
		this.price = price;
		this.status = status;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public Status getStatus() {
		return status;
	}
	
}
