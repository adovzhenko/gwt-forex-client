/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.trading.client.model;

import java.io.Serializable;
import java.util.Map;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class MatrixRowModel implements Serializable {
	
	private Tenor tenor;
	private Map<Tenor, MatrixRowModelCellValue> prices;

    @SuppressWarnings("unused")
    private MatrixRowModel(){}

    public MatrixRowModel(Tenor tenor, Map<Tenor, MatrixRowModelCellValue> prices) {
		this.tenor = tenor;
		this.prices = prices;
	}

	public Tenor getTenor() {
		return tenor;
	}

	public Map<Tenor, MatrixRowModelCellValue> getPrices() {
		return prices;
	}
	
}
