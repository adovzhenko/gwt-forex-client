/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.trading.client.model;

import java.io.Serializable;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class Account implements Serializable {
	
	private Long id;
	private String alias;
	private String feds;
	private String tcid;
	private String platform;
	
	@SuppressWarnings("unused")
	private Account() {}
	
	public Account(Long id, String alias, String feds, String tcid, String platform) {
		this.id = id;
		this.alias = alias;
		this.feds = feds;
		this.tcid = tcid;
		this.platform = platform;
	}
	
	public Long getId() {
		return id;
	}

	public String getAlias() {
		return alias;
	}

	public String getFeds() {
		return feds;
	}

	public String getTcid() {
		return tcid;
	}

	public String getPlatform() {
		return platform;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o != null && o instanceof Group) {
			return id.equals(((Account) o).id);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return id.hashCode();
	}

}
