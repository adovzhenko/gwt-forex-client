/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.trading.client.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class FXTradeRequest implements Serializable {

    private FXQuote fxQuote;
    private CurrencyPair currencyPair;
    private Tenor tenor;
    private Integer amountCcy1;
    private Integer amountCcy2;
    private Date date;
    private CcySide ccySide;

    @SuppressWarnings("unused")
    private FXTradeRequest() {}

    public FXTradeRequest(FXQuote fxQuote, CurrencyPair currencyPair, Tenor tenor
            , Integer amountCcy1, Integer amountCcy2, Date date, CcySide ccySide) {
        this.fxQuote = fxQuote;
        this.currencyPair = currencyPair;
        this.tenor = tenor;
        this.amountCcy1 = amountCcy1;
        this.amountCcy2 = amountCcy2;
        this.date = date;
        this.ccySide = ccySide;
    }

    public FXQuote getFxQuote() {
        return fxQuote;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public Tenor getTenor() {
        return tenor;
    }

    public Integer getAmountCcy1() {
        return amountCcy1;
    }

    public Integer getAmountCcy2() {
        return amountCcy2;
    }

    public Date getDate() {
        return date;
    }

    public CcySide getCcySide() {
        return ccySide;
    }
}
