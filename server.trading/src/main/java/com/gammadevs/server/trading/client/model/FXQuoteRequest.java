/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.trading.client.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class FXQuoteRequest implements Serializable {

    private CurrencyPair currencyPair;
    private Tenor tenor;
    private Integer amountCcy;
    private CcySide amountCcySide;
    private Date date;

    @SuppressWarnings("unused")
	private FXQuoteRequest() {}

    public FXQuoteRequest(CurrencyPair currencyPair, Tenor tenor
    		, Integer amountCcy, CcySide amountCcySide, Date date) {
        assert currencyPair != null;
        assert tenor != null;
        assert amountCcy != null;
        assert amountCcySide != null;
        assert date != null;
        this.currencyPair = currencyPair;
        this.tenor = tenor;
        this.amountCcy = amountCcy;
        this.amountCcySide = amountCcySide;
        this.date = date;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public Tenor getTenor() {
        return tenor;
    }

    public Integer getAmountCcy() {
		return amountCcy;
	}

	public CcySide getAmountCcySide() {
		return amountCcySide;
	}

	public Date getDate() {
        return date;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object != null && object instanceof FXQuoteRequest) {
            final FXQuoteRequest obj = (FXQuoteRequest) object;
            if (!currencyPair.equals(obj.currencyPair)) {
                return false;
            }
            if (tenor != obj.tenor) {
                return false;
            }
            if (!amountCcy.equals(obj.amountCcy)) {
                return false;
            }
            if (amountCcySide != obj.amountCcySide) {
                return false;
            }
            if (!date.equals(obj.date)) {
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return currencyPair.hashCode() + 31 * tenor.hashCode() + 31 * amountCcy.hashCode() + 31 * amountCcySide.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DealPanelModel: [ccyPair: ").append(currencyPair)
            .append("; tenor: ").append(tenor)
            .append("; amountCcy: ").append(amountCcy)
            .append("; amountCcySide: ").append(amountCcySide)
            .append("; date: ").append(date)
            .append("]");
        return sb.toString();
    }
}
