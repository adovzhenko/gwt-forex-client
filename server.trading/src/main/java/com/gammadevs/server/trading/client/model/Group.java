/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.trading.client.model;

import java.io.Serializable;
import java.util.List;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class Group implements Serializable {
	
	private Long id;
	private String name;
	private List<Account> accounts;
	
	@SuppressWarnings("unused")
	private Group() {}
	
	public Group(Long id, String name, List<Account> accounts) {
		assert id != null;
		assert name != null;
		this.id = id;
		this.name = name;
		this.accounts = accounts;
	}

	public String getName() {
		return name;
	}

	public List<Account> getAccounts() {
		return accounts;
	}
	
	public long getId() {
		return id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o != null && o instanceof Group) {
			return id.equals(((Group) o).id);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return id.hashCode();
	}

}
