/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.trading.client.model;

import java.io.Serializable;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class CurrencyPair implements Serializable, Comparable<CurrencyPair> {

    private Currency ccy1;
    private Currency ccy2;

    @SuppressWarnings("unused")
    private CurrencyPair() {}

    public CurrencyPair(Currency ccy1, Currency ccy2) {
        this.ccy1 = ccy1;
        this.ccy2 = ccy2;
    }

    public Currency getCcy1() {
        return ccy1;
    }

    public Currency getCcy2() {
        return ccy2;
    }

    @Override
    public int hashCode() {
        return (ccy1.getIso() + ccy2.getIso()).hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (object != null && object instanceof CurrencyPair) {
            return ccy1 == ((CurrencyPair) object).ccy1 && ccy2 == ((CurrencyPair) object).ccy2;
        }
        return false;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "[" + ccy1 + "/" + ccy2 + "]";
    }

	@Override
	public int compareTo(CurrencyPair o) {
		return (ccy1.getIso() + ccy2.getIso()).compareTo(o.ccy1.getIso() + o.ccy2.getIso());
	}

}
