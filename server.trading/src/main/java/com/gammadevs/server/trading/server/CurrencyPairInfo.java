/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.trading.server;

import java.math.BigDecimal;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class CurrencyPairInfo {

    private final BigDecimal buyPrice;
    private final BigDecimal sellPrice;
    private final int margin;

    public CurrencyPairInfo(BigDecimal sellPrice, BigDecimal buyPrice, int margin) {
        this.sellPrice = sellPrice;
        this.buyPrice = buyPrice;
        this.margin = margin;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public int getMargin() {
        return margin;
    }
}
