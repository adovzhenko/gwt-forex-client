/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.trading.client.model;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public enum Currency {
    USD("USD"), EUR("EUR"), CHF("CHF"), AUD("AUD");

    private String iso;

    Currency(String iso) {
        this.iso = iso;
    }

    public String getIso() {
        return iso;
    }
}
