/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.trading.client.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class FXTradeResponse implements Serializable {

    private long id;
    private FXTradeStatus fxOrderStatus;
    private CcySide ccySide;
    private CurrencyPair ccyPair;
    private Integer ccy1Amount;
    private Integer ccy2Amount;
    private Tenor tenor;
    private Date date;
    private BigDecimal price;

    @SuppressWarnings("unused")
    private FXTradeResponse() {}

    public FXTradeResponse(long id, FXTradeStatus fxOrderStatus, CcySide ccySide
            , CurrencyPair ccyPair, Integer ccy1Amount, Integer ccy2Amount
            , Tenor tenor, Date date, BigDecimal price) {
        this.id = id;
        this.fxOrderStatus = fxOrderStatus;
        this.ccySide = ccySide;
        this.ccyPair = ccyPair;
        this.ccy1Amount = ccy1Amount;
        this.ccy2Amount = ccy2Amount;
        this.tenor = tenor;
        this.date = date;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public FXTradeStatus getFxOrderStatus() {
        return fxOrderStatus;
    }

    public CcySide getCcySide() {
        return ccySide;
    }

    public CurrencyPair getCcyPair() {
        return ccyPair;
    }

    public Integer getCcy1Amount() {
		return ccy1Amount;
	}

	public Integer getCcy2Amount() {
		return ccy2Amount;
	}

	public Tenor getTenor() {
        return tenor;
    }

    public Date getDate() {
        return date;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
