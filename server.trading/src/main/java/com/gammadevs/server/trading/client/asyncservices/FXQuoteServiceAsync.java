/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.trading.client.asyncservices;

import com.gammadevs.server.trading.client.model.*;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public interface FXQuoteServiceAsync {

    void getFXQuote(FXQuoteRequest request, AsyncCallback<FXQuote> async);

    void submitFXTrade(FXTradeRequest request, AsyncCallback<FXTradeResponse> async);

    void getFXOrders(long lastAvailableOrderId, AsyncCallback<ArrayList<FXTradeResponse>> async);

    void getAllFXOrders(AsyncCallback<ArrayList<FXTradeResponse>> async);

    void getFXQuote(List<FXQuoteRequest> requests, AsyncCallback<ArrayList<FXQuote>> async);

    void getSwapMatrix(CurrencyPair currencyPair, int amount, AsyncCallback<ArrayList<MatrixRowModel>> async);
}
