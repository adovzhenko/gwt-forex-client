/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.trading.client.asyncservices;

import com.gammadevs.server.trading.client.model.*;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.ArrayList;
import java.util.List;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
@RemoteServiceRelativePath("FXQuoteService")
public interface FXQuoteService extends RemoteService {

    FXQuote getFXQuote(FXQuoteRequest request);
    ArrayList<FXQuote> getFXQuote(List<FXQuoteRequest> requests);
    FXTradeResponse submitFXTrade(FXTradeRequest request);
    ArrayList<FXTradeResponse> getAllFXOrders();
    ArrayList<FXTradeResponse> getFXOrders(long lastAvailableOrderId);
    ArrayList<MatrixRowModel> getSwapMatrix(CurrencyPair currencyPair, int amount);

}
