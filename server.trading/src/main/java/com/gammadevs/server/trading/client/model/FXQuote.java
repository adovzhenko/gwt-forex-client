/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.trading.client.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class FXQuote implements Serializable {

	private Date date;
    private BigDecimal buyPrice;
    private BigDecimal sellPrice;

    @SuppressWarnings("unused")
    private FXQuote() {}

    public FXQuote(BigDecimal buyPrice, BigDecimal sellPrice, Date date) {
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
        this.date = date;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }
    
    public Date getDate() {
		return date;
	}

	@Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(getClass().toString());
        sb.append("[buyPrice=").append(buyPrice).append("; ");
        sb.append("[sellPrice=").append(sellPrice).append("; ");
        sb.append("[date=").append(date).append("]");
        return sb.toString();
    }
}
