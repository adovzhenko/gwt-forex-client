/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.trading.server;

import com.gammadevs.server.trading.client.model.*;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import com.gammadevs.server.trading.client.asyncservices.FXQuoteService;

import javax.servlet.ServletException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class FXQuoteServiceImpl extends RemoteServiceServlet implements FXQuoteService {

    private final static Random RANDOM = new Random();
    private final static BigDecimal DIVIDER = new BigDecimal(1000000);
    private final static BigDecimal _10x3_DIVIDER = new BigDecimal(1000);
    private Map<CurrencyPair, CurrencyPairInfo> ccyPairsInfo;
    private List<FXTradeResponse> fxOrders = new CopyOnWriteArrayList<FXTradeResponse>();
    private AtomicLong orderCount;

    @Override
    public void init() throws ServletException {
        super.init();
        ccyPairsInfo = new HashMap<CurrencyPair, CurrencyPairInfo>();
        ccyPairsInfo.put(new CurrencyPair(Currency.AUD, Currency.CHF), new CurrencyPairInfo(new BigDecimal(0.845), new BigDecimal(0.851), 200));
        ccyPairsInfo.put(new CurrencyPair(Currency.AUD, Currency.EUR), new CurrencyPairInfo(new BigDecimal(0.741), new BigDecimal(0.752), 200));
        ccyPairsInfo.put(new CurrencyPair(Currency.AUD, Currency.USD), new CurrencyPairInfo(new BigDecimal(0.938), new BigDecimal(0.942), 200));
        
        ccyPairsInfo.put(new CurrencyPair(Currency.CHF, Currency.AUD), new CurrencyPairInfo(new BigDecimal(1.170), new BigDecimal(1.173), 150));
        ccyPairsInfo.put(new CurrencyPair(Currency.CHF, Currency.EUR), new CurrencyPairInfo(new BigDecimal(0.814), new BigDecimal(0.826), 150));
        ccyPairsInfo.put(new CurrencyPair(Currency.CHF, Currency.USD), new CurrencyPairInfo(new BigDecimal(0.911), new BigDecimal(0.923), 200));
        
        ccyPairsInfo.put(new CurrencyPair(Currency.EUR, Currency.USD), new CurrencyPairInfo(new BigDecimal(1.341), new BigDecimal(1.351), 200));
        ccyPairsInfo.put(new CurrencyPair(Currency.EUR, Currency.AUD), new CurrencyPairInfo(new BigDecimal(1.341), new BigDecimal(1.361), 150));
        ccyPairsInfo.put(new CurrencyPair(Currency.EUR, Currency.CHF), new CurrencyPairInfo(new BigDecimal(1.225), new BigDecimal(1.251), 150));
        
        ccyPairsInfo.put(new CurrencyPair(Currency.USD, Currency.AUD), new CurrencyPairInfo(new BigDecimal(1.063), new BigDecimal(1.071), 150));
        ccyPairsInfo.put(new CurrencyPair(Currency.USD, Currency.CHF), new CurrencyPairInfo(new BigDecimal(1.094), new BigDecimal(1.103), 150));
        ccyPairsInfo.put(new CurrencyPair(Currency.USD, Currency.EUR), new CurrencyPairInfo(new BigDecimal(0.744), new BigDecimal(0.749), 150));

        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        fxOrders.add(new FXTradeResponse(1, FXTradeStatus.EXECUTED, CcySide.SELL
                , new CurrencyPair(Currency.AUD, Currency.USD), 1000000, 93945, Tenor.SP, calendar.getTime(), new BigDecimal(0.93945)));
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        fxOrders.add(new FXTradeResponse(2, FXTradeStatus.EXECUTED, CcySide.SELL
                , new CurrencyPair(Currency.AUD, Currency.USD), 1005000, 93947, Tenor.TOM, calendar.getTime(), new BigDecimal(0.93947)));
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        fxOrders.add(new FXTradeResponse(3, FXTradeStatus.EXECUTED, CcySide.SELL
                , new CurrencyPair(Currency.AUD, Currency.USD), 50000, 93942 / 2, Tenor.SP, calendar.getTime(), new BigDecimal(0.93942)));
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        fxOrders.add(new FXTradeResponse(4, FXTradeStatus.FAILED, CcySide.BUY
                , new CurrencyPair(Currency.AUD, Currency.USD), 40000, 95945 * 2 / 5, Tenor.SP, calendar.getTime(), new BigDecimal(0.95945)));
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        fxOrders.add(new FXTradeResponse(5, FXTradeStatus.EXECUTED, CcySide.BUY
                , new CurrencyPair(Currency.AUD, Currency.USD), 20000, 96945 / 5, Tenor.TOM, calendar.getTime(), new BigDecimal(0.96945)));
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        fxOrders.add(new FXTradeResponse(6, FXTradeStatus.EXECUTED, CcySide.BUY
                , new CurrencyPair(Currency.AUD, Currency.USD), 70000, 97110 * 7 / 10, Tenor.TDY, calendar.getTime(), new BigDecimal(0.9711)));
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        fxOrders.add(new FXTradeResponse(7, FXTradeStatus.FAILED, CcySide.SELL
                , new CurrencyPair(Currency.AUD, Currency.USD), 2500000, 93955 * 5 / 2, Tenor.SP, calendar.getTime(), new BigDecimal(0.93955)));
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        fxOrders.add(new FXTradeResponse(8, FXTradeStatus.EXECUTED, CcySide.SELL
                , new CurrencyPair(Currency.AUD, Currency.USD), 100000, 93965, Tenor.TOM, calendar.getTime(), new BigDecimal(0.93965)));
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        fxOrders.add(new FXTradeResponse(9, FXTradeStatus.EXECUTED, CcySide.SELL
                , new CurrencyPair(Currency.AUD, Currency.USD), 200000, 93975 * 2, Tenor.SP, calendar.getTime(), new BigDecimal(0.93975)));
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        fxOrders.add(new FXTradeResponse(10, FXTradeStatus.FAILED, CcySide.SELL
                , new CurrencyPair(Currency.AUD, Currency.USD), 350000, 93980 * 7 / 2, Tenor.SP, calendar.getTime(), new BigDecimal(0.9398)));

        orderCount = new AtomicLong(fxOrders.size());
    }

    @Override
    public FXQuote getFXQuote(FXQuoteRequest request) {
        final CurrencyPairInfo info = ccyPairsInfo.get(request.getCurrencyPair());
        final FXQuote quote = new FXQuote(info.getBuyPrice().subtract(new BigDecimal(RANDOM.nextInt(info.getMargin())).divide(DIVIDER))
                , info.getSellPrice().add(new BigDecimal(RANDOM.nextInt(info.getMargin())).divide(DIVIDER))
                , new Date());
        return quote;
    }

    @Override
    public ArrayList<FXQuote> getFXQuote(List<FXQuoteRequest> requests) {
        final ArrayList<FXQuote> fxQuotes = new ArrayList<FXQuote>(requests.size());
        for (FXQuoteRequest request : requests) {
            fxQuotes.add(getFXQuote(request));
        }
        return fxQuotes;
    }

    @Override
    public FXTradeResponse submitFXTrade(FXTradeRequest request) {
        long orderCountValue = orderCount.incrementAndGet();
        final FXTradeStatus fxOrderStatus = orderCountValue % 5 != 0 && orderCountValue != 0 
        		? FXTradeStatus.EXECUTED : FXTradeStatus.FAILED;
        final FXTradeResponse response = new FXTradeResponse(orderCountValue
                , fxOrderStatus
                , request.getCcySide()
                , request.getCurrencyPair()
                , request.getAmountCcy1()
                , request.getAmountCcy2()
                , request.getTenor()
                , request.getDate()
                , request.getCcySide() == CcySide.SELL ? request.getFxQuote().getSellPrice() : request.getFxQuote().getBuyPrice());
        fxOrders.add(response);
        return response;
    }

    @Override
    public ArrayList<FXTradeResponse> getAllFXOrders() {
        return new ArrayList<FXTradeResponse>(fxOrders);
    }

    @Override
    public ArrayList<FXTradeResponse> getFXOrders(long lastAvailableOrderId) {
        ArrayList<FXTradeResponse> list = new ArrayList<FXTradeResponse>();
        for (FXTradeResponse item : fxOrders) {
            if (item.getId() > lastAvailableOrderId) {
                list.add(item);
            }
        }
        return list;
    }

    @Override
    //TODO: this method must not return MatrixRowModel entity, it must be visible only on client
    public ArrayList<MatrixRowModel> getSwapMatrix(CurrencyPair currencyPair, int amount) {
        final CurrencyPairInfo info = ccyPairsInfo.get(currencyPair);
        BigDecimal initialValue = info.getSellPrice().add(new BigDecimal(RANDOM.nextInt(info.getMargin())).divide(DIVIDER));
        final ArrayList<MatrixRowModel> dummyMatrixRowModels = new ArrayList<MatrixRowModel>(Tenor.values().length);
        for (Tenor tenor : Tenor.values()) {
            Map<Tenor, MatrixRowModelCellValue> prices = new HashMap<Tenor, MatrixRowModelCellValue>();
            MatrixRowModelCellValue.Status status = MatrixRowModelCellValue.Status.SELL;
            BigDecimal rowValue = initialValue.add(new BigDecimal(RANDOM.nextDouble()).divide(_10x3_DIVIDER));
            for (Tenor dependantTenor : Tenor.values()) {
                if (tenor == dependantTenor) {
                    prices.put(dependantTenor, new MatrixRowModelCellValue(null, MatrixRowModelCellValue.Status.NONE));
                    status = MatrixRowModelCellValue.Status.BUY;
                    continue;
                }
                prices.put(dependantTenor, new MatrixRowModelCellValue(rowValue.add(new BigDecimal(RANDOM.nextDouble()).divide(_10x3_DIVIDER)), status));
            }
            MatrixRowModel model = new MatrixRowModel(tenor, prices);
            dummyMatrixRowModels.add(model);
        }
        return dummyMatrixRowModels;
    }

}