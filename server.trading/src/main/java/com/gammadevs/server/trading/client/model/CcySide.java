/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.trading.client.model;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public enum CcySide {
    BUY, SELL;
}
