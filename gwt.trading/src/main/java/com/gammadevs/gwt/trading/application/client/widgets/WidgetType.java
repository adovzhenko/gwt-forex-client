/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.widgets;

public enum WidgetType {
	CCY_CHART
	, DEAL_PANEL
	, FX_TRADE_STATUS
	, MATRIX_VIEWER
	, ORDER_BLOTTER
	, RATE_VIEWER
	, TRADE_BLOTTER
}
