/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client;

import com.google.gwt.inject.client.AbstractGinModule;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;
import com.gammadevs.gwt.trading.application.client.model.cells.TenorCell;
import com.gammadevs.gwt.trading.application.client.providers.FXQuoteProvider;
import com.gammadevs.gwt.trading.application.client.resources.AppMessages;
import com.gammadevs.gwt.trading.application.client.resources.MainClientBundle;
import com.gammadevs.gwt.trading.application.client.storage.Storage;
import com.gammadevs.gwt.trading.application.client.widgets.MainLayout;
import com.gammadevs.gwt.trading.application.client.widgets.TradeBlotterView;
import com.gammadevs.gwt.trading.application.client.widgets.ccychart.CcyChartView;
import com.gammadevs.gwt.trading.application.client.widgets.matrixviewer.MatrixBlotterView;
import com.gammadevs.gwt.trading.application.client.widgets.rateviewer.RateViewerView;
import com.gammadevs.server.trading.client.asyncservices.FXQuoteServiceAsync;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class ApplicationGinModule extends AbstractGinModule {

    @Override
    protected void configure() {
        bind(EventBus.class).to(SimpleEventBus.class).in(Singleton.class);
        bind(MainClientBundle.class).in(Singleton.class);
        bind(AppMessages.class).in(Singleton.class);
        bind(FXQuoteServiceAsync.class).in(Singleton.class);

        bind(TradeBlotterView.class).in(Singleton.class);
        bind(MainLayout.class).in(Singleton.class);
        bind(MatrixBlotterView.class).in(Singleton.class);
        bind(RateViewerView.class).in(Singleton.class);
        bind(CcyChartView.class).in(Singleton.class);

        bind(TenorCell.class).in(Singleton.class);

        bind(FXQuoteProvider.class).in(Singleton.class);
        bind(DealPanelInstanceManager.class).in(Singleton.class);

        bind(Storage.class).in(Singleton.class);
    }

}
