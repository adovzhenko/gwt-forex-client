/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.storage;

import com.gammadevs.gwt.trading.application.client.model.RateViewerRowModel;
import com.gammadevs.server.trading.client.model.Account;
import com.gammadevs.server.trading.client.model.Currency;
import com.gammadevs.server.trading.client.model.CurrencyPair;
import com.gammadevs.server.trading.client.model.Group;
import com.gammadevs.server.trading.client.model.Tenor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public final class Storage {

    private final List<CurrencyPair> currencyPairs = new ArrayList<CurrencyPair>();
    private final List<RateViewerRowModel> defaultRateViewerRows = new ArrayList<RateViewerRowModel>();
    private final Date currentDate = new Date();

    public Storage() {
        init();
    }

    private void init() {
        currencyPairs.add(new CurrencyPair(Currency.AUD, Currency.CHF));
        currencyPairs.add(new CurrencyPair(Currency.AUD, Currency.EUR));
        currencyPairs.add(new CurrencyPair(Currency.AUD, Currency.USD));
        
        currencyPairs.add(new CurrencyPair(Currency.CHF, Currency.AUD));
        currencyPairs.add(new CurrencyPair(Currency.CHF, Currency.EUR));
        currencyPairs.add(new CurrencyPair(Currency.CHF, Currency.USD));
        
        currencyPairs.add(new CurrencyPair(Currency.EUR, Currency.AUD));
        currencyPairs.add(new CurrencyPair(Currency.EUR, Currency.CHF));
        currencyPairs.add(new CurrencyPair(Currency.EUR, Currency.USD));
        
        currencyPairs.add(new CurrencyPair(Currency.USD, Currency.AUD));
        currencyPairs.add(new CurrencyPair(Currency.USD, Currency.CHF));
        currencyPairs.add(new CurrencyPair(Currency.USD, Currency.EUR));
        
        defaultRateViewerRows.add(new RateViewerRowModel(currencyPairs.get(0), Tenor.SP, Tenor.SP.getValue(currentDate), 100000));
        defaultRateViewerRows.add(new RateViewerRowModel(currencyPairs.get(1), Tenor.SP, Tenor.SP.getValue(currentDate), 100000));
        defaultRateViewerRows.add(new RateViewerRowModel(currencyPairs.get(2), Tenor._1W, Tenor._1W.getValue(currentDate), 200000));
        defaultRateViewerRows.add(new RateViewerRowModel(currencyPairs.get(3), Tenor._1W, Tenor._1W.getValue(currentDate), 200000));
        defaultRateViewerRows.add(new RateViewerRowModel(currencyPairs.get(4), Tenor.SP, Tenor.SP.getValue(currentDate), 1500000));
        defaultRateViewerRows.add(new RateViewerRowModel(currencyPairs.get(5), Tenor.SP, Tenor.SP.getValue(currentDate), 3250000));
        defaultRateViewerRows.add(new RateViewerRowModel(currencyPairs.get(6), Tenor._1M, Tenor._1M.getValue(currentDate), 50000));
        defaultRateViewerRows.add(new RateViewerRowModel(currencyPairs.get(7), Tenor._1M, Tenor._1M.getValue(currentDate), 50000));
        defaultRateViewerRows.add(new RateViewerRowModel(currencyPairs.get(8), Tenor._1M, Tenor._1M.getValue(currentDate), 150000));
        defaultRateViewerRows.add(new RateViewerRowModel(currencyPairs.get(9), Tenor._2W, Tenor._2W.getValue(currentDate), 150000));
        defaultRateViewerRows.add(new RateViewerRowModel(currencyPairs.get(10), Tenor._2W, Tenor._2W.getValue(currentDate), 175000));
        defaultRateViewerRows.add(new RateViewerRowModel(currencyPairs.get(11), Tenor._2W, Tenor._2W.getValue(currentDate), 175000));
    }

    public List<CurrencyPair> getCurrencyPairs() {
        return currencyPairs;
    }

    public Date getCurrentDate() {
        return currentDate;
    }

	public List<RateViewerRowModel> getDefaultRateViewerRows() {
		return defaultRateViewerRows;
	}
	
}    
