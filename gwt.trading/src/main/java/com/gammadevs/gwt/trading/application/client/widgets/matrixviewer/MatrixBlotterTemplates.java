/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.widgets.matrixviewer;

import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public interface MatrixBlotterTemplates extends SafeHtmlTemplates {

	@Template("<div class='{0} {1}'>{2}</div>")
	SafeHtml td(String tdClassName, String tdBackground, String value);
}
