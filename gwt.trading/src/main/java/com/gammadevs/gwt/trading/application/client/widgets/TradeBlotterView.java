/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.widgets;

import com.gammadevs.gwt.common.client.callback.GwtLogAsyncCallback;
import com.gammadevs.gwt.common.client.mvp.IsMvpWidget;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.HasDirection.Direction;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.HasHorizontalAlignment.HorizontalAlignmentConstant;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.inject.Inject;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.SortDir;
import com.sencha.gxt.data.shared.Store;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.GridViewConfig;

import com.gammadevs.gwt.trading.application.client.Constants;
import com.gammadevs.gwt.trading.application.client.model.FXTradeResponseProperties;
import com.gammadevs.gwt.trading.application.client.model.FXTradeResponseWrapper;
import com.gammadevs.gwt.trading.application.client.model.cells.TenorCell;
import com.gammadevs.gwt.trading.application.client.resources.AppMessages;
import com.gammadevs.gwt.trading.application.client.resources.MainClientBundle;
import com.gammadevs.server.trading.client.asyncservices.FXQuoteServiceAsync;
import com.gammadevs.server.trading.client.model.CcySide;
import com.gammadevs.server.trading.client.model.CurrencyPair;
import com.gammadevs.server.trading.client.model.FXTradeResponse;
import com.gammadevs.server.trading.client.model.FXTradeStatus;
import com.gammadevs.server.trading.client.model.Tenor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class TradeBlotterView extends VerticalLayoutContainer implements IsMvpWidget {

    private final FXQuoteServiceAsync fxQuoteServiceAsync;
    private final AppMessages messages;
    private final MainClientBundle clientBundle;
    private final TenorCell tenorCell;

    @Inject
    private FXTradeResponseProperties props;

	private Grid<FXTradeResponseWrapper> grid;
	private List<FXTradeResponseWrapper> fxOrdersLastUpdate = new ArrayList<FXTradeResponseWrapper>(0);
	private long lastAvailableOrder = Long.MIN_VALUE;

    @Inject
    public TradeBlotterView(FXQuoteServiceAsync fxQuoteServiceAsync
            , AppMessages messages, MainClientBundle clientBundle, TenorCell tenorCell) {
        this.fxQuoteServiceAsync = fxQuoteServiceAsync;
        this.messages = messages;
        this.clientBundle = clientBundle;
        this.tenorCell = tenorCell;
        setBorders(false);
        fxQuoteServiceAsync.getAllFXOrders(new GwtLogAsyncCallback<ArrayList<FXTradeResponse>>(){
            @Override
            public void onSuccess(ArrayList<FXTradeResponse> result) {
                init(result);
                startUpdating();
            }
        });
    }

    @SuppressWarnings("unchecked")
	public void init(ArrayList<FXTradeResponse> orders) {

        final DateTimeFormat dateFormat = DateTimeFormat.getFormat(Constants.DATE_DEFAULT_PATTERN);

        ColumnConfig<FXTradeResponseWrapper, Long> idCol = 
        		new ColumnConfig<FXTradeResponseWrapper, Long>(props.id(), 60, messages.tradeBlotterColTradeId());
        ColumnConfig<FXTradeResponseWrapper, CurrencyPair> ccyPairCol = 
        		new ColumnConfig<FXTradeResponseWrapper, CurrencyPair>(props.ccyPair(), 75, messages.tradeBlotterColCcyPair());
        ColumnConfig<FXTradeResponseWrapper, String> ccy1Col = 
        		new ColumnConfig<FXTradeResponseWrapper, String>(props.ccy1(), 40, messages.tradeBlotterColCcy1());
        ColumnConfig<FXTradeResponseWrapper, String> ccy2Col = 
        		new ColumnConfig<FXTradeResponseWrapper, String>(props.ccy2(), 40, messages.tradeBlotterColCcy2());
        ColumnConfig<FXTradeResponseWrapper, CcySide> ccySideCol = 
        		new ColumnConfig<FXTradeResponseWrapper, CcySide>(props.ccySide(), 40, messages.tradeBlotterColSide());
        ColumnConfig<FXTradeResponseWrapper, FXTradeStatus> statusCol = 
        		new ColumnConfig<FXTradeResponseWrapper, FXTradeStatus>(props.fxOrderStatus(), 75, messages.tradeBlotterColStatus());
        ColumnConfig<FXTradeResponseWrapper, Integer> amount1Col = 
        		new ColumnConfig<FXTradeResponseWrapper, Integer>(props.getCcy1Amount(), 90, messages.tradeBlotterColCcy1Amount());
        ColumnConfig<FXTradeResponseWrapper, Integer> amount2Col = 
        		new ColumnConfig<FXTradeResponseWrapper, Integer>(props.getCcy2Amount(), 90, messages.tradeBlotterColCcy2Amount());
        ColumnConfig<FXTradeResponseWrapper, Tenor> tenorCol = 
        		new ColumnConfig<FXTradeResponseWrapper, Tenor>(props.tenor(), 40, messages.tradeBlotterColTenor());
        ColumnConfig<FXTradeResponseWrapper, Date> dateCol = 
        		new ColumnConfig<FXTradeResponseWrapper, Date>(props.date(), 75, messages.tradeBlotterColValueDate());
        ColumnConfig<FXTradeResponseWrapper, BigDecimal> priceCol = 
        		new ColumnConfig<FXTradeResponseWrapper, BigDecimal>(props.price(), 75, messages.tradeBlotterColPrice());

        dateCol.setCell(new AbstractCell<Date>() {
            @Override
            public void render(Context context, Date value, SafeHtmlBuilder sb) {
                sb.appendHtmlConstant(dateFormat.format(value));
            }
        });
        priceCol.setCell(new AbstractCell<BigDecimal>() {
            @Override
            public void render(Context context, BigDecimal value, SafeHtmlBuilder sb) {
                sb.appendHtmlConstant(Constants.QUOTE_NUMBER_FORMAT.format(value));
            }
        });
        priceCol.setAlignment(HorizontalAlignmentConstant.endOf(Direction.LTR));
        ccyPairCol.setCell(new AbstractCell<CurrencyPair>() {
            @Override
            public void render(Context context, CurrencyPair value, SafeHtmlBuilder sb) {
                sb.appendHtmlConstant(value.getCcy1().getIso() + "/" + value.getCcy2().getIso()	);
            }
        });
        amount1Col.setCell(new AbstractCell<Integer>() {
            @Override
            public void render(Context context, Integer value, SafeHtmlBuilder sb) {
                sb.appendHtmlConstant(Constants.CCY_AMOUNT_NUMBER_FORMAT.format(value));
            }
        });
        amount1Col.setAlignment(HorizontalAlignmentConstant.endOf(Direction.LTR));
        amount2Col.setCell(new AbstractCell<Integer>() {
            @Override
            public void render(Context context, Integer value, SafeHtmlBuilder sb) {
                sb.appendHtmlConstant(Constants.CCY_AMOUNT_NUMBER_FORMAT.format(value));
            }
        });
        amount2Col.setAlignment(HorizontalAlignmentConstant.endOf(Direction.LTR));
        tenorCol.setCell(tenorCell);

        List<ColumnConfig<FXTradeResponseWrapper, ?>> columnConfigs = new ArrayList<ColumnConfig<FXTradeResponseWrapper, ?>>();
        columnConfigs.addAll(Arrays.asList(idCol, ccyPairCol, ccySideCol, priceCol, statusCol, ccy1Col
        		, amount1Col, ccy2Col, amount2Col, tenorCol, dateCol));

        ColumnModel<FXTradeResponseWrapper> columnModel = new ColumnModel<FXTradeResponseWrapper>(columnConfigs);
        ListStore<FXTradeResponseWrapper> store = new ListStore<FXTradeResponseWrapper>(props.key());
        store.addSortInfo(new Store.StoreSortInfo<FXTradeResponseWrapper>(idCol.getValueProvider(), SortDir.DESC));
        
        grid = new Grid<FXTradeResponseWrapper>(store, columnModel);
        grid.setBorders(false);
        grid.getView().setStripeRows(true);
        grid.getView().setColumnLines(true);
        grid.setColumnReordering(true);
        grid.setStateful(true);

        grid.getView().setViewConfig(new GridViewConfig<FXTradeResponseWrapper>(){

			@Override
			public String getColStyle(FXTradeResponseWrapper model, ValueProvider<? super FXTradeResponseWrapper, ?> valueProvider,
					int rowIndex, int colIndex) {
				return "";
			}

			@Override
			public String getRowStyle(FXTradeResponseWrapper model, int rowIndex) {
				if (model.isNewData()) {
					return clientBundle.css().rowNew();
				}
				if (model.getFxOrderStatus() == FXTradeStatus.FAILED) {
					return clientBundle.css().rowFailed();
				}
				return "";
			}
		});

        this.setPixelSize(Constants.TRADE_BLOTTER_DIALOG_WIDTH - 15, Constants.TRADE_BLOTTER_DIALOG_HEIGHT - 15);
        this.add(grid, new VerticalLayoutData(1, 1));
        updateOrders(orders, false);
    }
    
    private void startUpdating() {
    	final Timer timer = new Timer() {

			@Override
			public void run() {
                fxQuoteServiceAsync.getFXOrders(getLastAvailableId()
						, new GwtLogAsyncCallback<ArrayList<FXTradeResponse>>(){

					@Override
					public void onSuccess(ArrayList<FXTradeResponse> result) {
						updateOrders(result, true);
					}					
				});
			}    		
    	};
    	timer.scheduleRepeating(Constants.TRADE_BLOTTER_UPDATE_INTERVAL);
    }
    
    private void updateOrders(ArrayList<FXTradeResponse> orders, boolean newData) {
    	for (FXTradeResponseWrapper order : fxOrdersLastUpdate) {
    		order.setNewData(false);
    		grid.getStore().update(order);
    	}
    	fxOrdersLastUpdate = new ArrayList<FXTradeResponseWrapper>();
    	for (FXTradeResponse order : orders) {
    		fxOrdersLastUpdate.add(new FXTradeResponseWrapper(newData, order));
    	}
    	grid.getStore().addAll(fxOrdersLastUpdate);
    }
    
    private long getLastAvailableId() {
    	for (FXTradeResponseWrapper fxOrder : grid.getStore().getAll()) {
    		if (fxOrder.getId() > lastAvailableOrder) {
    			lastAvailableOrder = fxOrder.getId();
    		}
    	}
    	return lastAvailableOrder;
    }

    @Override
    public void start(HasWidgets container) {
        container.add(this);
    }

    @Override
    public void stop() {}

}