/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.model;

import com.gammadevs.server.trading.client.model.MatrixRowModel;
import com.gammadevs.server.trading.client.model.Tenor;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public interface MatrixRowModelProperties extends PropertyAccess<MatrixRowModel> {

	@Editor.Path("tenor")
    ModelKeyProvider<MatrixRowModel> key();
	ValueProvider<MatrixRowModel, Tenor> tenor();

}
