/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.widgets.ccychart;

import java.util.Date;
import java.util.Random;

import com.gammadevs.gwt.common.client.mvp.IsMvpWidget;
import com.google.gwt.user.client.ui.HasWidgets;
import com.gammadevs.gwt.trading.application.client.Constants;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArrayMixed;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.googlecode.gwt.charts.client.ChartLoader;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.CandlestickChart;
import com.googlecode.gwt.charts.client.corechart.CandlestickChartOptions;
import com.googlecode.gwt.charts.client.options.Legend;
import com.googlecode.gwt.charts.client.options.LegendPosition;
import com.googlecode.gwt.charts.client.options.Tooltip;
import com.googlecode.gwt.charts.client.util.ChartHelper;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class CcyChartView implements IsMvpWidget {

	private final Widget rootElement;
    private HasWidgets container;
	
	@UiField
	SimpleContainer chartContainer;

	public CcyChartView() {
		rootElement = ourUiBinder.createAndBindUi(this);
		initGoogleChartLoader();
	}
	
	private void initGoogleChartLoader() {
		ChartLoader chartLoader = new ChartLoader(ChartPackage.CORECHART);
		chartLoader.loadApi(new Runnable() {
			@Override
			public void run() {
				initGoogleChart();
			}
		});
	}
	
	private void initGoogleChart() {
		
		String chartData = getMockChartData();
		GWT.log(chartData);	
		
		final CandlestickChart chart = new CandlestickChart();
		JsArrayMixed dataArray = JsonUtils.unsafeEval(chartData);
		DataTable dataTable = ChartHelper.arrayToDataTable(dataArray, true);
		CandlestickChartOptions options = CandlestickChartOptions.create();
		options.setLegend(Legend.create(LegendPosition.NONE));
		options.setTooltip(Tooltip.create());
		chart.draw(dataTable, options);
			
		chartContainer.setWidget(chart);
	}
	
	private String getMockChartData() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		final Date now = new Date();
		int numberOfDays = 30;
		double middlePrice = 1.35;
		double previousDayClosed = middlePrice;
		final Random random = new Random();
		for (int i = 0; i < numberOfDays; i++) {
			Date quoteDate = CalendarUtil.copyDate(now); 
			CalendarUtil.addDaysToDate(quoteDate, - (numberOfDays - i));
			sb.append("[");
			String date = i % 5 == 0 ? Constants.DATE_CCY_CHART_FORMAT.format(quoteDate) : "";
			sb.append("'").append(date).append("'").append(",");
			double lowest = 0;
			double open = 0;
			double close = 0;
			double upper = 0;
			if (i < 7) { //UP
				lowest = previousDayClosed - random.nextDouble() / 100;
				open = previousDayClosed;
				close = open + random.nextDouble() / 100;
				upper = close + random.nextDouble() / 100 * (i % 2);
			} else if (i < 16) { //DOWN
				open = previousDayClosed;
				upper = open + random.nextDouble() / 100;
				close = previousDayClosed - random.nextDouble() / 100 * 2;
				lowest = close - random.nextDouble() / 100 * (i % 3);
				
			} else if (i < 22) { //UP
				lowest = previousDayClosed - random.nextDouble() / 100;
				open = previousDayClosed;
				close = open + random.nextDouble() / 100 * 2;
				upper = close + random.nextDouble() / 100 * (i % 4);
			} else { //DOWN
				open = previousDayClosed;
				upper = open + random.nextDouble() / 100;
				close = previousDayClosed - random.nextDouble() / 100;
				lowest = close - random.nextDouble() / 100 * (i % 2);
			}
			previousDayClosed = close;
			String lowestFormatted = Constants.QUOTE_SHORT_NUMBER_FORMAT.format(lowest); 
			String openFormatted = Constants.QUOTE_SHORT_NUMBER_FORMAT.format(open); 
			String closeFormatted = Constants.QUOTE_SHORT_NUMBER_FORMAT.format(close); 
			String upperFormatted = Constants.QUOTE_SHORT_NUMBER_FORMAT.format(upper); 
			sb.append(lowestFormatted).append(",");
			sb.append(openFormatted).append(",");
			sb.append(closeFormatted).append(",");
			sb.append(upperFormatted);
			
			
			sb.append("]");
			sb.append(",");
			
		}
		sb.append("]");
		return sb.toString();
	}

    @Override
    public void start(HasWidgets container) {
        this.container = container;
        container.add(rootElement);
    }

    @Override
    public void stop() {
    	container.remove(rootElement);
        container = null;
    }

    interface CcyChartViewUiBinder extends UiBinder<Widget, CcyChartView> {}
	private static CcyChartViewUiBinder ourUiBinder = GWT.create(CcyChartViewUiBinder.class);

}