/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.model;

import java.math.BigDecimal;
import java.util.Date;

import com.gammadevs.server.trading.client.model.CcySide;
import com.gammadevs.server.trading.client.model.CurrencyPair;
import com.gammadevs.server.trading.client.model.FXTradeStatus;
import com.gammadevs.server.trading.client.model.Tenor;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public interface FXTradeResponseProperties extends PropertyAccess<FXTradeResponseWrapper> {

    @Editor.Path("id")
    ModelKeyProvider<FXTradeResponseWrapper> key();
    ValueProvider<FXTradeResponseWrapper, Long> id();
    ValueProvider<FXTradeResponseWrapper, FXTradeStatus> fxOrderStatus();
    ValueProvider<FXTradeResponseWrapper, CcySide> ccySide();
    ValueProvider<FXTradeResponseWrapper, CurrencyPair> ccyPair();
    ValueProvider<FXTradeResponseWrapper, String> ccy1();
    ValueProvider<FXTradeResponseWrapper, String> ccy2();
    ValueProvider<FXTradeResponseWrapper, Integer> getCcy1Amount();
    ValueProvider<FXTradeResponseWrapper, Integer> getCcy2Amount();
    ValueProvider<FXTradeResponseWrapper, Tenor> tenor();
    ValueProvider<FXTradeResponseWrapper, Date> date();
    ValueProvider<FXTradeResponseWrapper, BigDecimal> price();
    ValueProvider<FXTradeResponseWrapper, Boolean> newData();

}
