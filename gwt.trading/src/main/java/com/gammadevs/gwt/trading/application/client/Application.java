/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client;

import com.gammadevs.gwt.trading.application.client.events.ShowCcyChartEvent;
import com.gammadevs.gwt.trading.application.client.events.ShowDealPanelEvent;
import com.gammadevs.gwt.trading.application.client.events.ShowRateViewerEvent;
import com.gammadevs.gwt.trading.application.client.events.ShowTradeBlotterEvent;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.*;
import com.google.web.bindery.event.shared.EventBus;

import com.gammadevs.gwt.trading.application.client.model.DealPanelModel;
import com.gammadevs.server.trading.client.model.Currency;
import com.gammadevs.server.trading.client.model.CurrencyPair;
import com.gammadevs.server.trading.client.model.Tenor;

import java.util.Date;

/**
 * Forex GWT portal POC.
 * Modules architecture follows http://www.gwtproject.org/articles/mvp-architecture.html
 * But views are not separated on view and presenter for development speed purposes.
 * All other approaches are similar to given reference.
 * @author Anton Dovzhenko
 */
public class Application implements EntryPoint {
    public void onModuleLoad() {
        final ApplicationController controller = new ApplicationController();
        controller.go(RootLayoutPanel.get());
        showDefaultWorkspace(controller.getEventBus());
    }

    private void showDefaultWorkspace(EventBus eventBus) {
        final Date date = new Date();
        DealPanelModel model = new DealPanelModel(new CurrencyPair(Currency.EUR, Currency.USD), Tenor.SP, 100000, null, Tenor.SP.getValue(date));
        eventBus.fireEvent(new ShowDealPanelEvent(model));
        model = new DealPanelModel(new CurrencyPair(Currency.AUD, Currency.USD), Tenor.TOM, 200000, null, Tenor.TOM.getValue(date));
        eventBus.fireEvent(new ShowDealPanelEvent(model));
        model = new DealPanelModel(new CurrencyPair(Currency.EUR, Currency.CHF), Tenor._1W, 30000, null, Tenor._1W.getValue(date));
        eventBus.fireEvent(new ShowDealPanelEvent(model));
        model = new DealPanelModel(new CurrencyPair(Currency.CHF, Currency.USD), Tenor.SP, 150000, null, Tenor.SP.getValue(date));
        eventBus.fireEvent(new ShowDealPanelEvent(model));
        eventBus.fireEvent(new ShowTradeBlotterEvent(true, true));
        eventBus.fireEvent(new ShowRateViewerEvent(true, true));
        eventBus.fireEvent(new ShowCcyChartEvent(true, true));
    }
    
}
