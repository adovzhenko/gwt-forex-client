/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import com.gammadevs.gwt.trading.application.client.model.DealPanelModel;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class ShowDealPanelEvent extends GwtEvent<ShowDealPanelEvent.Handler> {

    private final DealPanelModel model;

    public ShowDealPanelEvent(DealPanelModel model) {
        this.model = model;
    }

    public DealPanelModel getModel() {
        return model;
    }

    public interface Handler extends EventHandler {
        void handle(ShowDealPanelEvent event);
    }
    public static Type<Handler> TYPE = new Type<Handler>();

    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(Handler handler) {
        handler.handle(this);
    }

}
