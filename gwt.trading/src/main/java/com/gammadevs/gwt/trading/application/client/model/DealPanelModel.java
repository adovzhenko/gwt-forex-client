/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.model;

import java.io.Serializable;
import java.util.Date;

import com.gammadevs.server.trading.client.model.CurrencyPair;
import com.gammadevs.server.trading.client.model.Tenor;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class DealPanelModel implements Serializable {

    private CurrencyPair currencyPair;
    private Tenor tenor;
    private Integer amountCcy1;
    private Integer amountCcy2;
    private Date date;

    public DealPanelModel() {}

    public DealPanelModel(CurrencyPair currencyPair, Tenor tenor
    		, Integer amountCcy1, Integer amountCcy2, Date date) {
        this.currencyPair = currencyPair;
        this.tenor = tenor;
        this.amountCcy1 = amountCcy1;
        this.amountCcy2 = amountCcy2;
        this.date = date;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public Tenor getTenor() {
        return tenor;
    }

    public Integer getAmountCcy1() {
		return amountCcy1;
	}

	public Integer getAmountCcy2() {
		return amountCcy2;
	}

	public void setAmountCcy1(Integer amountCcy1) {
		this.amountCcy1 = amountCcy1;
	}

	public void setAmountCcy2(Integer amountCcy2) {
		this.amountCcy2 = amountCcy2;
	}

	public Date getDate() {
        return date;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object != null && object instanceof DealPanelModel) {
            final DealPanelModel obj = (DealPanelModel) object;
            if (currencyPair != obj.currencyPair && !currencyPair.equals(obj.currencyPair)) {
                return false;
            }
            if (tenor != obj.tenor) {
                return false;
            }
            if (!(amountCcy1 == obj.amountCcy1 || (amountCcy1 != null && amountCcy1.equals(obj.amountCcy1)))) {
                return false;
            }
            if (!(amountCcy2 == obj.amountCcy2 || (amountCcy2 != null && amountCcy2.equals(obj.amountCcy2)))) {
                return false;
            }
            if (date != obj.date && !date.equals(obj.date)) {
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DealPanelModel: [ccyPair: ").append(currencyPair)
            .append("; tenor: ").append(tenor)
            .append("; amountCcy1: ").append(amountCcy1)
            .append("; amountCcy2: ").append(amountCcy2)
            .append("; date: ").append(date)
            .append("]");
        return sb.toString();
    }
}
