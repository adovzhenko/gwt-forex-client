/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.widgets;

import com.gammadevs.gwt.common.client.mvp.IsMvpWidget;
import com.gammadevs.server.trading.client.model.CcySide;
import com.gammadevs.server.trading.client.model.CurrencyPair;
import com.gammadevs.server.trading.client.model.FXTradeResponse;
import com.gammadevs.server.trading.client.model.FXTradeStatus;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasWidgets;

import com.google.inject.Inject;
import com.gammadevs.gwt.trading.application.client.Constants;
import com.gammadevs.gwt.trading.application.client.resources.AppMessages;
import com.gammadevs.gwt.trading.application.client.resources.MainClientBundle;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class FXTradeStatusView implements IsMvpWidget {

    private final HTMLPanel rootElement;
    private final AppMessages messages;
    private final MainClientBundle clientBundle;

    @UiField
    DivElement tradeStatusField;
    @UiField
    DivElement mainInfoDiv;
    @UiField
    SpanElement mainInfoAmountSpan;
    @UiField
    SpanElement mainInfoTenorSpan;
    @UiField
    SpanElement dealDateField;
    @UiField
    SpanElement quoteField;

    @Inject
    public FXTradeStatusView(MainClientBundle clientBundle, AppMessages messages) {
        this.messages = messages;
        this.clientBundle = clientBundle;
        rootElement = ourUiBinder.createAndBindUi(this);
    }

    public void setFXTradeResponse(FXTradeResponse response) {
        final CurrencyPair ccyPair = response.getCcyPair();
        tradeStatusField.setInnerHTML(messages.fxOrderStatusViewStatusField(getTradeStatus(response.getFxOrderStatus())
                , ccyPair.getCcy1().getIso()));
        mainInfoDiv.addClassName(getMainInfoDivClassName(response.getFxOrderStatus(), response.getCcySide()));
        mainInfoTenorSpan.setInnerHTML(response.getTenor().getName());
        mainInfoAmountSpan.setInnerHTML(messages.fxOrderStatusViewAmountField(ccyPair.getCcy1().getIso()
                , ccyPair.getCcy2().getIso(), response.getCcy1Amount()));
        dealDateField.setInnerHTML(DateTimeFormat.getFormat(Constants.DATE_DEFAULT_PATTERN).format(response.getDate()));
        quoteField.setInnerHTML(Constants.QUOTE_NUMBER_FORMAT.format(response.getPrice()));
    }

    private String getTradeStatus(FXTradeStatus fxTradeStatus) {
        switch (fxTradeStatus) {
            case EXECUTED:
                return "COMPLETED";
            case FAILED:
                return "REJECTED";
        }
        throw new IllegalArgumentException("No status defined for " +  fxTradeStatus);
    }

    private String getMainInfoDivClassName(FXTradeStatus fxOrderStatus, CcySide ccySide) {
        if (fxOrderStatus == FXTradeStatus.FAILED) {
            return clientBundle.css().rejectedBackground();
        } else {
            if (ccySide == CcySide.SELL) {
                return clientBundle.css().sellBackground();
            } else {
                return clientBundle.css().buyBackground();
            }
        }
    }

    @Override
    public void start(HasWidgets container) {
        container.add(rootElement);
    }

    @Override
    public void stop() {}

    interface FXOrderStatusViewUiBinder extends UiBinder<HTMLPanel, FXTradeStatusView> {}
    private static FXOrderStatusViewUiBinder ourUiBinder = GWT.create(FXOrderStatusViewUiBinder.class);

}