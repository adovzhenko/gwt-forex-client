/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.i18n.shared.DateTimeFormat;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public interface Constants {

    int FX_SPOT_DIALOG_WIDTH = 200;
    int FX_SPOT_DIALOG_HEIGHT = 270;
    int FX_SPOT_DIALOG_MAX_INSTANCES = 10;
    int FX_SPOT_DIALOG_HIDE_ANIMATION_TIME = 1000;

    int FX_QUOTE_TICK_PERIOD_IN_MS = 2000;

    int TRADE_BLOTTER_DIALOG_WIDTH = 600;
    int TRADE_BLOTTER_DIALOG_HEIGHT = 300;
    int TRADE_BLOTTER_UPDATE_INTERVAL = 5000;
    
    int MATRIX_BLOTTER_COLUMN_WIDTH = 50;

    int FX_ORDER_STATUS_VIEW_WIDTH = 200;
    int FX_ORDER_STATUS_VIEW_HEIGHT = 150;
    int FX_ORDER_STATUS_VIEW_LIVING_TIME = 5000;
    int FX_ORDER_STATUS_VIEW_HIDE_ANIMATION_TIME = 5000;
    
    int RATE_VIEWER_GROUP_COMBO_LIST_WIDTH = 300;
    int RATE_VIEWER_ROW_HIDE_ANIMATION_TIME = 500;

    String DATE_DEFAULT_PATTERN = "MMM dd, yyyy";
    String DATE_CCY_CHART_PATTERN = "dd MMM";
    String QUOTE_PATTERN = "0.00000";
    String QUOTE_SHORT_PATTERN = "0.0000";

    NumberFormat CCY_AMOUNT_NUMBER_FORMAT = NumberFormat.getFormat("###,###,###,###.00");
    NumberFormat QUOTE_NUMBER_FORMAT = NumberFormat.getFormat(QUOTE_PATTERN);
    NumberFormat QUOTE_SHORT_NUMBER_FORMAT = NumberFormat.getFormat(QUOTE_SHORT_PATTERN);
    
    DateTimeFormat DATE_CCY_CHART_FORMAT = DateTimeFormat.getFormat(DATE_CCY_CHART_PATTERN);

 
}
