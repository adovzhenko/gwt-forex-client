/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client;

import com.google.inject.Inject;
import com.gammadevs.gwt.trading.application.client.resources.AppMessages;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class DealPanelInstanceManager {

    private final AppMessages messages;

	private int dialogCount;
	private boolean[] dialogInstances = new boolean[Constants.FX_SPOT_DIALOG_MAX_INSTANCES];

    @Inject
    public DealPanelInstanceManager(AppMessages messages) {
        this.messages = messages;
    }
	
	public int getNextDealPanelNumber() {
    	for (int i = 0; i < dialogInstances.length; i++) {
    		if (!dialogInstances[i]) {
    			dialogInstances[i] = true;
    			return i + 1;
    		}
    	}
    	throw new IllegalStateException(messages.exceptionNoNewInstances());
    }
	
	public int addInstanceAndGetNumber() {
		dialogCount++;
		return getNextDealPanelNumber();
	}
	
	public void removeInstance(int instanceNumber) {
		dialogCount--;
		dialogInstances[instanceNumber - 1] = false;
	}
	
	public boolean isNewInstanceAvailable() {
		return dialogCount < Constants.FX_SPOT_DIALOG_MAX_INSTANCES;
	}
	
}
