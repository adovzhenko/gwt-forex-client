/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client;

import com.gammadevs.gwt.common.client.utils.DialogUtils;
import com.gammadevs.gwt.common.client.utils.Function;
import com.gammadevs.server.trading.client.model.FXTradeResponse;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import com.sencha.gxt.core.client.Style.LayoutRegion;
import com.sencha.gxt.fx.client.FxElement;
import com.sencha.gxt.fx.client.animation.Fx;
import com.sencha.gxt.widget.core.client.Portlet;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.info.Info;

import com.gammadevs.gwt.trading.application.client.events.DeleteRateViewerRowEvent;
import com.gammadevs.gwt.trading.application.client.events.ShowCcyChartEvent;
import com.gammadevs.gwt.trading.application.client.events.ShowDealPanelEvent;
import com.gammadevs.gwt.trading.application.client.events.ShowFXTradeStatusEvent;
import com.gammadevs.gwt.trading.application.client.events.ShowMatrixBlotterEvent;
import com.gammadevs.gwt.trading.application.client.events.ShowRateViewerEvent;
import com.gammadevs.gwt.trading.application.client.events.ShowTradeBlotterEvent;
import com.gammadevs.gwt.trading.application.client.model.DealPanelModel;
import com.gammadevs.gwt.trading.application.client.resources.AppMessages;
import com.gammadevs.gwt.trading.application.client.resources.MainClientBundle;
import com.gammadevs.gwt.trading.application.client.widgets.DealPanelView;
import com.gammadevs.gwt.trading.application.client.widgets.FXTradeStatusView;
import com.gammadevs.gwt.trading.application.client.widgets.MainLayout;
import com.gammadevs.gwt.trading.application.client.widgets.TradeBlotterView;
import com.gammadevs.gwt.trading.application.client.widgets.ccychart.CcyChartView;
import com.gammadevs.gwt.trading.application.client.widgets.matrixviewer.MatrixBlotterView;
import com.gammadevs.gwt.trading.application.client.widgets.rateviewer.RateViewerView;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class ApplicationController {

    private final ApplicationGinjector injector = GWT.create(ApplicationGinjector.class);

    private final EventBus eventBus;
    private final MainLayout mainLayout;
    private final AppMessages messages;
    private final MainClientBundle clientBundle;
    
    private TradeBlotterView tradeBlotterView;
    private MatrixBlotterView matrixBlotterView;
    private RateViewerView rateViewerView;
    private CcyChartView ccyChartView;
    
    private DealPanelInstanceManager dealPanelInstanceManager = injector.getDealPanelInstanceManager();

    ApplicationController() {
        eventBus = injector.getEventBus();
        mainLayout = injector.getMainLayout();
        messages = injector.getAppMessages();
        clientBundle = injector.getMainClientBundle();
        initEventBusListeners();
    }

    private void initEventBusListeners() {
    	initWidgetAppearanceListeners();
        initRateViewerListeners();
        initSouthRegionListeners();
    }
    
    private void initWidgetAppearanceListeners() {
    	 eventBus.addHandler(ShowDealPanelEvent.TYPE, new ShowDealPanelEvent.Handler(){
             @Override
             public void handle(ShowDealPanelEvent event) {
             	showDealPanelWidget(event.getModel());
             }
         });
         eventBus.addHandler(ShowFXTradeStatusEvent.TYPE, new ShowFXTradeStatusEvent.Handler() {
             @Override
             public void handle(ShowFXTradeStatusEvent event) {
             	showFXTradeStatusView(event.getParent(), event.getFxOrderResponse());
             }
         });
         eventBus.addHandler(ShowTradeBlotterEvent.TYPE, new ShowTradeBlotterEvent.Handler(){
             @Override
             public void handle(ShowTradeBlotterEvent event) {
             	showTradeBlotter(event.isEnabled(), event.isFiredManually());
             }
         });
         eventBus.addHandler(ShowRateViewerEvent.TYPE, new ShowRateViewerEvent.Handler(){
             @Override
             public void handle(ShowRateViewerEvent event) {
             	showRateViewer(event.isEnabled(), event.isFiredManually());
             }
         });
         eventBus.addHandler(ShowCcyChartEvent.TYPE, new ShowCcyChartEvent.Handler(){
 			@Override
 			public void handle(ShowCcyChartEvent event) {
 				showCcyChart(event.isEnabled(), event.isFiredManually());
 			}
 		});
    }
    
    private void initRateViewerListeners() {
    	eventBus.addHandler(DeleteRateViewerRowEvent.TYPE, new DeleteRateViewerRowEvent.Handler() {			
			@Override
			public void handle(DeleteRateViewerRowEvent event) {
				rateViewerView.removeRow(event.getRow());
			}
		});
    }
    
    private void initSouthRegionListeners() {
    	eventBus.addHandler(ShowMatrixBlotterEvent.TYPE, new ShowMatrixBlotterEvent.Handler() {			
			@Override
			public void handle(ShowMatrixBlotterEvent event) {
				if (matrixBlotterView == null) {
					matrixBlotterView = injector.getMatrixBlotterView();
				}
                matrixBlotterView.start(mainLayout.getMatrixBlotterContainer());
			}
		});
    }
    
    private void showTradeBlotter(boolean enabled, boolean firedManually) {
    	if (firedManually) {
    		mainLayout.setTradeBlotterMenuItemValue(enabled);
    	}
    	if (enabled) {
    		if (tradeBlotterView == null) {
        		tradeBlotterView = injector.getTradeBlotterView();
        	}
    		mainLayout.getBlotterContainer().setWidget(tradeBlotterView);
    		mainLayout.getLayoutContainer().expand(LayoutRegion.SOUTH, false);
    	} else {
    		if (tradeBlotterView != null) {
    			mainLayout.getBlotterContainer().remove(tradeBlotterView);
    			mainLayout.getLayoutContainer().collapse(LayoutRegion.SOUTH, false);
    		}
    	}
    }
    
    private void showRateViewer(boolean enabled, boolean firedManually) {
    	if (firedManually) {
    		mainLayout.setRateViewerMenuItemValue(enabled);
    	}
    	if (enabled) {
    		if (rateViewerView == null) {
    			rateViewerView = injector.getRateViewerView();
                rateViewerView.initDefaultRows(injector.getStorage().getDefaultRateViewerRows());
        	}
            rateViewerView.start(mainLayout.getRateViewerContainer());
    		mainLayout.getLayoutContainer().expand(LayoutRegion.WEST, false);
    	} else {
    		if (rateViewerView != null) {
                rateViewerView.stop();
    			mainLayout.getLayoutContainer().collapse(LayoutRegion.WEST, false);
    		}
    	}
    }
    
    private void showCcyChart(boolean enabled, boolean firedManually) {
    	if (firedManually) {
    		mainLayout.setChartsMenuItemValue(enabled);
    	}
    	if (enabled) {
    		if (ccyChartView == null) {
    			ccyChartView = injector.getCcyChartView();
        	}
            ccyChartView.start(mainLayout.getChartContainer());
    		mainLayout.getCenterLayoutContainer().expand(LayoutRegion.SOUTH, false);
    	} else {
    		if (ccyChartView != null) {
                ccyChartView.stop();
    			mainLayout.getCenterLayoutContainer().collapse(LayoutRegion.SOUTH, false);
    		}
    	}
    }
    
    private void showDealPanelWidget(DealPanelModel model) {
    	if (!dealPanelInstanceManager.isNewInstanceAvailable()) {
    		Info.display(messages.warning()
    				, messages.dealPanelInnstancesLimitExceed(Constants.FX_SPOT_DIALOG_MAX_INSTANCES));
    	} else {
	    	final Portlet dialog = new Portlet();
            dialog.addStyleName(clientBundle.css().dealPanelPortlet());
	    	final int instanceNumber = dealPanelInstanceManager.addInstanceAndGetNumber();
            final DealPanelView view = injector.getDealPanelView();
            view.setWindowAndInitModel(dialog, model);
            view.start(dialog);
	        dialog.setWidth(Constants.FX_SPOT_DIALOG_WIDTH);
	        dialog.setHeight(Constants.FX_SPOT_DIALOG_HEIGHT);
	        dialog.setHeadingText(messages.dealPanelName(instanceNumber));
	        dialog.setPinned(true);
	        DialogUtils.addCloseButton(dialog, new Function() {
                @Override
                public void execute() {
                    view.stop();
                    dialog.getElement().<FxElement>cast().fadeToggle(new Fx(Constants.FX_SPOT_DIALOG_HIDE_ANIMATION_TIME) {
                        @Override
                        public void onComplete() {
                            dialog.removeFromParent();
                            dealPanelInstanceManager.removeInstance(instanceNumber);
                            super.onComplete();
                        }
                    });
                }
            });
	        mainLayout.getPortal().add(dialog);
    	}
    }
    
    private void showFXTradeStatusView(Widget parent, FXTradeResponse fxOrderResponse) {
    	final Window dialog = new Window();
        final FXTradeStatusView view = injector.getFXTradeStatusView();
        view.setFXTradeResponse(fxOrderResponse);
        view.start(dialog);
        dialog.setBounds(parent.getAbsoluteLeft() + Constants.FX_SPOT_DIALOG_WIDTH + 5
                , parent.getAbsoluteTop() + 5
                , Constants.FX_ORDER_STATUS_VIEW_WIDTH
                , Constants.FX_ORDER_STATUS_VIEW_HEIGHT);
        dialog.show();
        new Timer() {
            @Override
            public void run() {
                view.stop();
                dialog.setAnimationDuration(Constants.FX_ORDER_STATUS_VIEW_HIDE_ANIMATION_TIME);
                dialog.hide();
            }
        }.schedule(Constants.FX_ORDER_STATUS_VIEW_LIVING_TIME);
    }

    public void go(HasWidgets container) {
        clientBundle.css().ensureInjected();
        mainLayout.start(container);
    }

    EventBus getEventBus() {
        return eventBus;
    }
    
}
