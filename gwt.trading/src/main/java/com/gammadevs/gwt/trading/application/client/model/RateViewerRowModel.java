/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.model;

import java.util.Date;

import com.gammadevs.server.trading.client.model.CurrencyPair;
import com.gammadevs.server.trading.client.model.Tenor;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class RateViewerRowModel {

	private final CurrencyPair currencyPair;
    private final Tenor tenor;
    private final Integer amountCcy;
    private final Date tenorDate;
    
    public RateViewerRowModel(CurrencyPair currencyPair, Tenor tenor
    		, Date tenorDate, Integer amountCcy) {
    	this.currencyPair = currencyPair;
    	this.tenor = tenor;
    	this.tenorDate = tenorDate;
    	this.amountCcy = amountCcy;
    }

	public CurrencyPair getCurrencyPair() {
		return currencyPair;
	}

	public Tenor getTenor() {
		return tenor;
	}

	public Integer getAmountCcy() {
		return amountCcy;
	}

	public Date getTenorDate() {
		return tenorDate;
	}
    
}
