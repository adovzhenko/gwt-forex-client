package com.gammadevs.gwt.trading.application.client.widgets.rateviewer;

import java.util.List;

import com.gammadevs.gwt.common.client.mvp.IsMvpWidget;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.inject.Inject;
import com.google.inject.Provider;

import com.gammadevs.gwt.trading.application.client.model.RateViewerRowModel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;


/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class RateViewerView implements IsMvpWidget {
	
	private final VerticalLayoutData ROW_LAYOUT_DATA = new VerticalLayoutData(1, 0.1, new Margins(1, 2, 1, 2));
    private final Widget rootElement;
    private final Provider<RateViewerRowView> rateViewerRowViewProvider;

    private HasWidgets container;
	
	@UiField
	VerticalLayoutContainer verticalContainer;

    @Inject
	public RateViewerView(Provider<RateViewerRowView> rateViewerRowViewProvider) {
		this.rateViewerRowViewProvider = rateViewerRowViewProvider;
		rootElement = uiBinder.createAndBindUi(this);
	}
	
	public void initDefaultRows(List<RateViewerRowModel> defaultRateViewerRows) {
		for (RateViewerRowModel rateViewerRowModel : defaultRateViewerRows) {
            final RateViewerRowView rowView = rateViewerRowViewProvider.get();
            rowView.setModel(rateViewerRowModel);
            rowView.setLayoutData(ROW_LAYOUT_DATA);
            rowView.start(verticalContainer);
		}
	}
	
	public void removeRow(final RateViewerRowView row) {
        row.stop();
	}

    @Override
    public void start(HasWidgets container) {
        this.container = container;
        container.add(rootElement);
    }

    @Override
    public void stop() {
    	container.remove(rootElement);
        container = null;
    }

    private static RateViewerViewUiBinder uiBinder = GWT.create(RateViewerViewUiBinder.class);
    interface RateViewerViewUiBinder extends UiBinder<Widget, RateViewerView> {}

}
