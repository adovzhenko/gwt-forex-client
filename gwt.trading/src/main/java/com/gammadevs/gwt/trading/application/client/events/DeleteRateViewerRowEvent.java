/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import com.gammadevs.gwt.trading.application.client.widgets.rateviewer.RateViewerRowView;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class DeleteRateViewerRowEvent extends GwtEvent<DeleteRateViewerRowEvent.Handler> {

    private final RateViewerRowView row;

    public DeleteRateViewerRowEvent(RateViewerRowView row) {
        this.row = row;
    }

    public RateViewerRowView getRow() {
        return row;
    }

    public interface Handler extends EventHandler {
        void handle(DeleteRateViewerRowEvent event);
    }
    public static Type<Handler> TYPE = new Type<Handler>();

    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(Handler handler) {
        handler.handle(this);
    }

}
