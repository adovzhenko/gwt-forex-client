/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.widgets;

import com.gammadevs.gwt.common.client.mvp.IsMvpWidget;
import com.gammadevs.gwt.common.client.sencha.BorderLayoutContainerExtended;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.PlainTabPanel;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.button.ToggleButton;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.event.CollapseItemEvent;
import com.sencha.gxt.widget.core.client.event.CollapseItemEvent.CollapseItemHandler;
import com.sencha.gxt.widget.core.client.event.ExpandItemEvent;
import com.sencha.gxt.widget.core.client.event.ExpandItemEvent.ExpandItemHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.menu.Item;
import com.sencha.gxt.widget.core.client.menu.MenuItem;

import com.gammadevs.gwt.trading.application.client.events.ShowCcyChartEvent;
import com.gammadevs.gwt.trading.application.client.events.ShowDealPanelEvent;
import com.gammadevs.gwt.trading.application.client.events.ShowMatrixBlotterEvent;
import com.gammadevs.gwt.trading.application.client.events.ShowRateViewerEvent;
import com.gammadevs.gwt.trading.application.client.events.ShowTradeBlotterEvent;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class MainLayout implements IsMvpWidget {

    private final Widget rootElement;
    private final EventBus eventBus;

    @UiField
    TextButton menuExecutionFxSpot;
    @UiField
    MenuItem menuSettingsPreferences;
    @UiField
    MenuItem menuHelpAbout;
    @UiField
    ToggleButton menuExecutionTradeBlotter;
    @UiField
    ToggleButton menuExecutionRateViewer;
    @UiField
    ToggleButton menuCharts;
    @UiField
    FlowPanel portal;
    @UiField
    ContentPanel rateViewerContainer;
    @UiField
    ContentPanel southContainer;
    @UiField
    ContentPanel chartContainer;
    @UiField
    SimpleContainer tradeBlotterContainer;
    @UiField
    SimpleContainer matrixBlotterContainer;
    @UiField
    PlainTabPanel southTabPanel;
    @UiField
    BorderLayoutContainerExtended layoutContainer;
    @UiField
    BorderLayoutContainerExtended centerLayoutContainer;

    @Inject
    public MainLayout(EventBus eventBus) {
        this.eventBus = eventBus;
        rootElement = ourUiBinder.createAndBindUi(this);
        southContainer.collapse();
        rateViewerContainer.collapse();
        chartContainer.collapse();
        initListeners();
    }
    
    protected void initListeners() {
    	layoutContainer.addExpandHandler(new ExpandItemHandler<ContentPanel>(){
			@Override
			public void onExpand(ExpandItemEvent<ContentPanel> event) {
				if (event.getItem() == southContainer) {
					eventBus.fireEvent(new ShowTradeBlotterEvent(true, true));
				} else if (event.getItem() == rateViewerContainer) {
					eventBus.fireEvent(new ShowRateViewerEvent(true, true));
				}
			}
		});
    	layoutContainer.addCollapseHandler(new CollapseItemHandler<ContentPanel>(){
			@Override
			public void onCollapse(CollapseItemEvent<ContentPanel> event) {
				if (event.getItem() == southContainer) {
					eventBus.fireEvent(new ShowTradeBlotterEvent(false, true));
				} else if (event.getItem() == rateViewerContainer) {
					eventBus.fireEvent(new ShowRateViewerEvent(false, true));
				}
			}
		});
    	centerLayoutContainer.addExpandHandler(new ExpandItemHandler<ContentPanel>(){
			@Override
			public void onExpand(ExpandItemEvent<ContentPanel> event) {
				if (event.getItem() == chartContainer) {
					eventBus.fireEvent(new ShowCcyChartEvent(true, true));
				}
			}
		});
    	centerLayoutContainer.addCollapseHandler(new CollapseItemHandler<ContentPanel>(){
			@Override
			public void onCollapse(CollapseItemEvent<ContentPanel> event) {
				if (event.getItem() == chartContainer) {
					eventBus.fireEvent(new ShowCcyChartEvent(false, true));
				}
			}
		});
    	menuExecutionTradeBlotter.addValueChangeHandler(new ValueChangeHandler<Boolean>(){
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				eventBus.fireEvent(new ShowTradeBlotterEvent(event.getValue(), false));
			}
		});
    	menuExecutionRateViewer.addValueChangeHandler(new ValueChangeHandler<Boolean>(){
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				eventBus.fireEvent(new ShowRateViewerEvent(event.getValue(), false));
			}
		});
    	menuExecutionFxSpot.addSelectHandler(new SelectHandler(){
			@Override
			public void onSelect(SelectEvent event) {
				eventBus.fireEvent(new ShowDealPanelEvent(null));
			}
		});
    	menuCharts.addValueChangeHandler(new ValueChangeHandler<Boolean>(){
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				eventBus.fireEvent(new ShowCcyChartEvent(event.getValue(), false));
			}
		});
    }

    @UiHandler(value = {"menuSettingsPreferences"})
    public void onMenuSettingsPreferencesSelection(SelectionEvent<Item> event) {
        MenuItem item = (MenuItem) event.getSelectedItem();
        Info.display("Action", "You selected the " + item.getText());
    }

    @UiHandler("menuHelpAbout")
    public void onMenuHelpSelection(SelectionEvent<Item> event) {
        final AlertMessageBox d = new AlertMessageBox("Do you want to bring Trading to WEB?"
                , "Contact anthony300787@gmail.com for cooperation.");
        d.show();
    }

    public FlowPanel getPortal() {
        return portal;
    }

    public SimpleContainer getBlotterContainer() {
		return tradeBlotterContainer;
	}

	public SimpleContainer getMatrixBlotterContainer() {
		return matrixBlotterContainer;
	}

	public ContentPanel getRateViewerContainer() {
		return rateViewerContainer;
	}
	
	public ContentPanel getChartContainer() {
		return chartContainer;
	}

	public BorderLayoutContainerExtended getLayoutContainer() {
		return layoutContainer;
	}
	
	public BorderLayoutContainerExtended getCenterLayoutContainer() {
		return centerLayoutContainer;
	}

	public void setTradeBlotterMenuItemValue(boolean value) {
		menuExecutionTradeBlotter.setValue(value);
	}
	
	public void setRateViewerMenuItemValue(boolean value) {
		menuExecutionRateViewer.setValue(value);
	}
	
	public void setChartsMenuItemValue(boolean value) {
		menuCharts.setValue(value);
	}

    @Override
    public void start(HasWidgets container) {
        container.add(rootElement);
    }

    @Override
    public void stop() {}

    interface MainLayoutUiBinder extends UiBinder<Widget, MainLayout> {}
    private static MainLayoutUiBinder ourUiBinder = GWT.create(MainLayoutUiBinder.class);

}