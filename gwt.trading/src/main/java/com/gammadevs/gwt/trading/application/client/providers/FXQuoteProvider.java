/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.providers;

import com.gammadevs.gwt.common.client.callback.GwtLogAsyncCallback;
import com.gammadevs.server.trading.client.asyncservices.FXQuoteServiceAsync;
import com.gammadevs.server.trading.client.model.FXQuote;
import com.gammadevs.server.trading.client.model.FXQuoteRequest;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Timer;
import com.google.inject.Inject;

import com.gammadevs.gwt.trading.application.client.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class FXQuoteProvider {

    private final Map<FXQuoteRequest, List<Listener>> quoteListeners = new HashMap<FXQuoteRequest, List<Listener>>();
    private final Timer timer;
    private int listenersCount;

    @Inject
    public FXQuoteProvider(final FXQuoteServiceAsync fxQuoteServiceAsync) {
        timer = new Timer() {
            @Override
            public void run() {
                final List<FXQuoteRequest> requests = new ArrayList<FXQuoteRequest>(quoteListeners.keySet());
                fxQuoteServiceAsync.getFXQuote(requests, new GwtLogAsyncCallback<ArrayList<FXQuote>>() {
                    @Override
                    public void onSuccess(ArrayList<FXQuote> result) {
                        for (int i = 0; i < requests.size(); i++) {
                            List<Listener> listeners = quoteListeners.get(requests.get(i));
                            if (listeners != null) {
                                for (Listener listener : listeners) {
                                    listener.onUpdate(result.get(i));
                                }
                            }
                        }
                    }
                });
            }
        };
        timer.scheduleRepeating(Constants.FX_QUOTE_TICK_PERIOD_IN_MS);
    }

    public boolean addListener(FXQuoteRequest request, Listener listener) {
        List<Listener> listeners = quoteListeners.get(request);
        if (listeners == null) {
            listeners = new ArrayList<Listener>();
            quoteListeners.put(request, listeners);
        }
        boolean result = listeners.add(listener);
        if (result) {
        	GWT.log("FXQuoteProvider: listenersCount=" + (++listenersCount) + "; requestsCount=" + quoteListeners.size());
        }
        return result;
    }

    public boolean removeListener(FXQuoteRequest request, Listener listener) {
        List<Listener> listeners = quoteListeners.get(request);
        if (listeners != null) {
        	boolean result = listeners.remove(listener);
        	if (listeners.isEmpty()) {
        		quoteListeners.remove(request);
        	}
        	if (result) {
        		GWT.log("FXQuoteProvider: listenersCount=" + (--listenersCount) + "; requestsCount=" + quoteListeners.size());
        	}
            return result;
        }
        return false;
    }

    public static interface Listener {
        void onUpdate(FXQuote value);
    }
}
