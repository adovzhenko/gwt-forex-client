/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.resources;

import com.google.gwt.i18n.client.Messages;
import com.gammadevs.server.trading.client.model.CcySide;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public interface AppMessages extends Messages {

    String dealPanelName(int instanceNumber);

    String tradeBlotterName();

    String tradeBlotterColTradeId();

    String tradeBlotterColCcyPair();
    
    String tradeBlotterColCcy1();
    
    String tradeBlotterColCcy2();

    String tradeBlotterColSide();

    String tradeBlotterColStatus();

    String tradeBlotterColCcy1Amount();
    
    String tradeBlotterColCcy2Amount();

    String tradeBlotterColTenor();

    String tradeBlotterColValueDate();

    String tradeBlotterColPrice();

    String ccyComboBoxText();

    @DefaultMessage("Sell {0}")
    @AlternateMessage({"BUY", "Buy {0}"})
    String dealPanelCcy1OperationFieldText(String iso, @Select CcySide ccySide);

    @DefaultMessage("Buy {0}")
    @AlternateMessage({"BUY", "Sell {0}"})
    String dealPanelCcy2OperationFieldText(String iso, @Select CcySide ccySide);
    
    String dealPanelInnstancesLimitExceed(int maxNumberOfInstances);

    String fxOrderStatusViewStatusField(String status, String ccy1);

    String fxOrderStatusViewAmountField(String ccy1, String ccy2, Integer amount);
    
    String rateViewerGroupCBText();
    
    String rateViewerAccountCBText();
    
    String warning();
    
    String exceptionNoNewInstances();
}
