/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.widgets;

import com.gammadevs.server.trading.client.asyncservices.FXQuoteServiceAsync;
import com.gammadevs.server.trading.client.model.*;
import com.gammadevs.server.trading.client.asyncservices.FXQuoteServiceAsync;
import com.gammadevs.server.trading.client.model.*;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.widget.core.client.form.*;

import com.gammadevs.gwt.trading.application.client.Constants;
import com.gammadevs.gwt.trading.application.client.events.ShowFXTradeStatusEvent;
import com.gammadevs.gwt.trading.application.client.model.DealPanelModel;
import com.gammadevs.gwt.trading.application.client.model.DealPanelModelValidator;
import com.gammadevs.gwt.trading.application.client.model.FXQuoteModel;
import com.gammadevs.gwt.trading.application.client.providers.FXQuoteProvider;
import com.gammadevs.gwt.trading.application.client.resources.AppMessages;
import com.gammadevs.gwt.trading.application.client.resources.MainClientBundle;
import com.gammadevs.gwt.trading.application.client.storage.Storage;
import com.gammadevs.gwt.common.client.mvp.IsMvpWidget;
import com.gammadevs.gwt.common.client.callback.GwtLogAsyncCallback;

import java.util.Arrays;
import java.util.Date;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class DealPanelView implements IsMvpWidget, FXQuoteProvider.Listener {

    private final HTMLPanel rootElement;
    private final EventBus eventBus;
    private final FXQuoteProvider fxQuoteProvider;
    private final AppMessages messages;
    private final MainClientBundle clientBundle;
    private final FXQuoteServiceAsync fxQuoteServiceAsync;
    private final Storage storage;

    private Widget parentWindow;
    private DealPanelModel currentModel;
    private FXQuoteRequest currentFXQuoteRequest;
    private FXQuote fxQuote = null;
    private CcySide ccySide = CcySide.SELL;

    @UiField(provided = true)
    SimpleComboBox<CurrencyPair> ccyComboBox;
    @UiField(provided = true)
    SimpleComboBox<Tenor> tenorComboBox;
    @UiField
    DateField dateField;
    @UiField(provided=true)
    NumberField<Integer> ccy1NumberField;
    @UiField(provided=true)
    NumberField<Integer> ccy2NumberField;

    @UiField
    DivElement sellButton;
    @UiField
    DivElement buyButton;

    @UiField
    DivElement buyPriceMajor;
    @UiField
    DivElement buyPriceMiddle;
    @UiField
    DivElement buyPriceMinor;
    @UiField
    DivElement buyPrice;
    @UiField
    DivElement buyMargin;

    @UiField
    DivElement sellPriceMajor;
    @UiField
    DivElement sellPriceMiddle;
    @UiField
    DivElement sellPriceMinor;
    @UiField
    DivElement sellPrice;
    @UiField
    DivElement sellMargin;

    @UiField
    DivElement ccy1OperationField;
    @UiField
    DivElement ccy2OperationField;

    @Inject
    public DealPanelView(EventBus eventBus, FXQuoteProvider fxQuoteProvider
            , FXQuoteServiceAsync fxQuoteServiceAsync, Storage storage
            , AppMessages messages, MainClientBundle clientBundle) {
        this.eventBus = eventBus;
        this.fxQuoteProvider = fxQuoteProvider;
        this.fxQuoteServiceAsync = fxQuoteServiceAsync;
        this.storage = storage;
        this.messages = messages;
        this.clientBundle = clientBundle;
        ccyComboBox = new SimpleComboBox<CurrencyPair>(new LabelProvider<CurrencyPair>(){
            @Override
            public String getLabel(CurrencyPair item) {
                return item.getCcy1().getIso() + "/" + item.getCcy2().getIso();
            }
        });
        ccyComboBox.add(storage.getCurrencyPairs());
        ccyComboBox.setEmptyText(messages.ccyComboBoxText());
        tenorComboBox = new SimpleComboBox<Tenor>(new LabelProvider<Tenor>() {
            @Override
            public String getLabel(Tenor item) {
                return item.getName();
            }
        });
        tenorComboBox.setWidth(70);
        tenorComboBox.add(Arrays.asList(Tenor.values()));
        tenorComboBox.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        ccy1NumberField = new NumberField<Integer>(new NumberPropertyEditor.IntegerPropertyEditor());
        ccy1NumberField.setFormat(Constants.CCY_AMOUNT_NUMBER_FORMAT);
        ccy1NumberField.setWidth(93);
        ccy2NumberField = new NumberField<Integer>(new NumberPropertyEditor.IntegerPropertyEditor());
        ccy2NumberField.setFormat(Constants.CCY_AMOUNT_NUMBER_FORMAT);
        ccy2NumberField.setWidth(93);
        rootElement = ourUiBinder.createAndBindUi(this);

        dateField.setPropertyEditor(new DateTimePropertyEditor(Constants.DATE_DEFAULT_PATTERN));
        initListeners();
    }

    public void setWindowAndInitModel(Widget parentWindow, DealPanelModel model) {
        this.parentWindow = parentWindow;
        if (model != null) {
            setModel(model);
            ccyComboBox.setValue(model.getCurrencyPair());
            tenorComboBox.setValue(model.getTenor());
            if (model.getAmountCcy1() != null) {
                ccy1NumberField.setValue(model.getAmountCcy1());
            }
            if (model.getAmountCcy2() != null) {
                ccy2NumberField.setValue(model.getAmountCcy2());
            }
            dateField.setValue(model.getDate());
        }
    }

    protected void initListeners() {
        ccyComboBox.addSelectionHandler(new SelectionHandler<CurrencyPair>(){
            @Override
            public void onSelection(SelectionEvent<CurrencyPair> event) {
                handleModelChange(ccy1Active());
            }
        });
        tenorComboBox.addSelectionHandler(new SelectionHandler<Tenor>(){
            @Override
            public void onSelection(SelectionEvent<Tenor> event) {
                if (event.getSelectedItem() != null) {
                    dateField.setValue(event.getSelectedItem().getValue(storage.getCurrentDate()));
                }
                handleModelChange(ccy1Active());
            }
        });
        dateField.getDatePicker().addValueChangeHandler(new ValueChangeHandler<Date>() {
            @Override
            public void onValueChange(ValueChangeEvent<Date> event) {
                handleModelChange(ccy1Active());
            }
        });
        ccy1NumberField.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                handleModelChange(true);
            }
        });
        ccy2NumberField.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                handleModelChange(false);
            }
        });
        ccy1NumberField.addHandler(new ClickHandler(){
            @Override
            public void onClick(ClickEvent event) {
                ccy1NumberField.setEnabled(true);
                ccy2NumberField.setEnabled(false);
            }
        }, ClickEvent.getType());
        ccy2NumberField.addHandler(new ClickHandler(){
            @Override
            public void onClick(ClickEvent event) {
                ccy1NumberField.setEnabled(false);
                ccy2NumberField.setEnabled(true);
            }
        }, ClickEvent.getType());
        DOM.setEventListener(sellButton.<com.google.gwt.user.client.Element>cast(), new EventListener() {
            @Override
            public void onBrowserEvent(Event event) {
                if (event.getTypeInt() == Event.ONMOUSEOVER) {
                    handleCcySideChange(CcySide.SELL);
                } else if (event.getTypeInt() == Event.ONCLICK) {
                    submitOrder(CcySide.SELL);
                }
            }
        });
        DOM.sinkEvents(sellButton.<com.google.gwt.user.client.Element>cast(), Event.ONMOUSEOVER | Event.ONCLICK);
        DOM.setEventListener(buyButton.<com.google.gwt.user.client.Element>cast(), new EventListener() {
            @Override
            public void onBrowserEvent(Event event) {
                if (event.getTypeInt() == Event.ONMOUSEOVER) {
                    handleCcySideChange(CcySide.BUY);
                } else if (event.getTypeInt() == Event.ONCLICK) {
                    submitOrder(CcySide.BUY);
                }
            }
        });
        DOM.sinkEvents(buyButton.<com.google.gwt.user.client.Element>cast(), Event.ONMOUSEOVER | Event.ONCLICK);
    }

    private void handleCcySideChange(CcySide ccySide) {
        if (ccySide != this.ccySide) {
            this.ccySide = ccySide;
            final boolean currencyPairAvailable = isCurrencyPairAvailable();
            final String ccy1 = currencyPairAvailable ? currentModel.getCurrencyPair().getCcy1().getIso() : "";
            final String ccy2 = currencyPairAvailable ? currentModel.getCurrencyPair().getCcy2().getIso() : "";
            ccy1OperationField.setInnerHTML(messages.dealPanelCcy1OperationFieldText(ccy1, ccySide));
            ccy2OperationField.setInnerHTML(messages.dealPanelCcy2OperationFieldText(ccy2, ccySide));
            final String classToRemove = ccySide == CcySide.SELL ?
                    clientBundle.css().buyBackgroundActive() : clientBundle.css().sellBackgroundActive();
            final String classToAdd = ccySide == CcySide.SELL ?
                    clientBundle.css().sellBackgroundActive() : clientBundle.css().buyBackgroundActive();
            ccy1OperationField.removeClassName(classToRemove);
            ccy2OperationField.removeClassName(classToRemove);
            ccy1OperationField.addClassName(classToAdd);
            ccy2OperationField.addClassName(classToAdd);
            if (isAmountAndQuoteAvailable()) {
                calculateCc2Amount();
            }
        }
    }
    
    private boolean ccy1Active() {
    	return ccy1NumberField.isEnabled(); 
    }

    private boolean isCurrencyPairAvailable() {
        return currentModel != null && currentModel.getCurrencyPair() != null;
    }

    private boolean isAmountAndQuoteAvailable() {
    	final boolean ccy1Active = ccy1Active();
        return currentModel != null && fxQuote != null
        		&& (ccy1Active && currentModel.getAmountCcy1() != null || !ccy1Active && currentModel.getAmountCcy1() != null);
    }

    private void calculateCc2Amount() {
    	final boolean ccy1Active = ccy1Active();
    	if (currentModel.getAmountCcy1() != null && ccy1Active) {
    		final int amountCcy2 = (int) (currentModel.getAmountCcy1() *
	                (ccySide == CcySide.SELL ? fxQuote.getSellPrice().doubleValue() : fxQuote.getBuyPrice().doubleValue()));
    		currentModel.setAmountCcy2(amountCcy2);
	        ccy2NumberField.setValue(amountCcy2);
    	} else if (currentModel.getAmountCcy2() != null && !ccy1Active) {
    		final int amountCcy1 = (int) (currentModel.getAmountCcy2() /
	                (ccySide == CcySide.SELL ? fxQuote.getSellPrice().doubleValue() : fxQuote.getBuyPrice().doubleValue()));
    		currentModel.setAmountCcy1(amountCcy1);
    		ccy1NumberField.setValue(amountCcy1);
    	}
    }

    private void handleModelChange(boolean ccy1Active) {
        final DealPanelModel model = new DealPanelModel(
                ccyComboBox.getCurrentValue()
                , tenorComboBox.getCurrentValue()
                , ccy1Active ? ccy1NumberField.getCurrentValue() : null
                , ccy1Active ? null : ccy2NumberField.getCurrentValue()
                , dateField.getCurrentValue()
        );
        setModel(model);
    }

    private void setModel(final DealPanelModel model) {
    	if (currentFXQuoteRequest != null) {
            fxQuoteProvider.removeListener(currentFXQuoteRequest, this);
        }
        if (DealPanelModelValidator.isValid(model) && (currentModel == null || !currentModel.equals(model))) {
            ccy1OperationField.setInnerHTML(
                    messages.dealPanelCcy1OperationFieldText(model.getCurrencyPair().getCcy1().getIso(), CcySide.SELL));
            ccy2OperationField.setInnerHTML(
                    messages.dealPanelCcy2OperationFieldText(model.getCurrencyPair().getCcy2().getIso(), CcySide.SELL));
            clear();
            currentModel = model;
            final boolean ccy1Active = ccy1Active();
            currentFXQuoteRequest = new FXQuoteRequest(currentModel.getCurrencyPair()
            		, currentModel.getTenor()
            		, ccy1Active ? currentModel.getAmountCcy1() : currentModel.getAmountCcy2()
            		, ccy1Active ? CcySide.SELL : CcySide.BUY
            		, currentModel.getDate());
            fxQuoteProvider.addListener(currentFXQuoteRequest, this);
        }
    }

    private void submitOrder(final CcySide ccySide) {
        if (currentModel != null
                && DealPanelModelValidator.isValid(currentModel)
                && fxQuote != null) {
            fxQuoteServiceAsync.submitFXTrade(new FXTradeRequest(fxQuote, currentModel.getCurrencyPair(), currentModel.getTenor()
                    , currentModel.getAmountCcy1(), currentModel.getAmountCcy2(), currentModel.getDate(), ccySide)
                    , new GwtLogAsyncCallback<FXTradeResponse>() {

                @Override
                public void onSuccess(FXTradeResponse result) {
                    eventBus.fireEvent(new ShowFXTradeStatusEvent(parentWindow, result));
                }
            });
        }
    }

    private void clear() {
        removeCurrentQuoteRequest();
        buyPriceMajor.setInnerHTML("");
        buyPriceMiddle.setInnerHTML("");
        buyPriceMinor.setInnerHTML("");
        buyPrice.setInnerHTML("");
        buyMargin.setInnerHTML("");

        sellPriceMajor.setInnerHTML("");
        sellPriceMiddle.setInnerHTML("");
        sellPriceMinor.setInnerHTML("");
        sellPrice.setInnerHTML("");
        sellMargin.setInnerHTML("");
    }

    private void removeCurrentQuoteRequest() {
        if (currentFXQuoteRequest != null) {
            fxQuoteProvider.removeListener(currentFXQuoteRequest, this);
        }
    }

    @Override
    public void onUpdate(FXQuote value) {
        fxQuote = value;
        final FXQuoteModel fxQuoteModel = new FXQuoteModel(fxQuote);

        buyPrice.setInnerHTML(fxQuoteModel.getBuyPrice());
        buyPriceMajor.setInnerHTML(fxQuoteModel.getBuyPriceMajor());
        buyPriceMiddle.setInnerHTML(fxQuoteModel.getBuyPriceMiddle());
        buyPriceMinor.setInnerHTML(fxQuoteModel.getBuyPriceMinor());

        sellPrice.setInnerHTML(fxQuoteModel.getSellPrice());
        sellPriceMajor.setInnerHTML(fxQuoteModel.getSellPriceMajor());
        sellPriceMiddle.setInnerHTML(fxQuoteModel.getSellPriceMiddle());
        sellPriceMinor.setInnerHTML(fxQuoteModel.getSellPriceMinor());

        calculateCc2Amount();
    }

    @Override
    public void start(HasWidgets container) {
        container.add(rootElement);
    }

    @Override
    public void stop() {
        DOM.sinkEvents(sellButton.<com.google.gwt.user.client.Element>cast(), 0);
        DOM.sinkEvents(buyButton.<com.google.gwt.user.client.Element>cast(), 0);
        ccyComboBox.setEnabled(false);
        tenorComboBox.setEnabled(false);
        dateField.setEnabled(false);
        ccy1NumberField.setEnabled(false);
        ccy2NumberField.setEnabled(false);
        removeCurrentQuoteRequest();
    }

    interface DealPanelViewUiBinder extends UiBinder<HTMLPanel, DealPanelView> {}
    private static DealPanelViewUiBinder ourUiBinder = GWT.create(DealPanelViewUiBinder.class);

}