/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.widgets.matrixviewer;

import java.util.ArrayList;
import java.util.List;

import com.gammadevs.gwt.common.client.callback.GwtLogAsyncCallback;
import com.gammadevs.gwt.common.client.mvp.IsMvpWidget;
import com.gammadevs.server.trading.client.asyncservices.FXQuoteServiceAsync;
import com.gammadevs.server.trading.client.model.*;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.inject.Inject;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.form.NumberField;
import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor;
import com.sencha.gxt.widget.core.client.form.SimpleComboBox;
import com.gammadevs.gwt.trading.application.client.Constants;
import com.gammadevs.gwt.trading.application.client.model.MatrixRowValueProvider;
import com.gammadevs.gwt.trading.application.client.model.cells.TenorCell;
import com.gammadevs.gwt.trading.application.client.model.MatrixRowModelProperties;
import com.gammadevs.gwt.trading.application.client.resources.AppMessages;
import com.gammadevs.gwt.trading.application.client.resources.MainClientBundle;
import com.gammadevs.gwt.trading.application.client.storage.Storage;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class MatrixBlotterView implements IsMvpWidget {

    private final VerticalLayoutContainer rootElement;
    private final FXQuoteServiceAsync fxQuoteServiceAsync;
    private final Storage storage;
    private final TenorCell tenorCell;
    private final AppMessages messages;
    private final MainClientBundle clientBundle;
    private final MatrixRowModelProperties props;
    private final MatrixBlotterTemplates templates;

    private List<ColumnConfig<MatrixRowModel, ?>> columnConfigs;
    private ListStore<MatrixRowModel> store;

    @UiField(provided=true)
    Grid<MatrixRowModel> matrixGrid;
    @UiField(provided=true)
    SimpleComboBox<CurrencyPair> ccyComboBox;
    @UiField(provided=true)
    NumberField<Integer> amountNumberField;
    @UiField
    TextButton recalculateButton;

    @Inject
	public MatrixBlotterView(FXQuoteServiceAsync fxQuoteServiceAsync, TenorCell tenorCell
            , Storage storage, AppMessages messages, MainClientBundle clientBundle
            , MatrixRowModelProperties props, MatrixBlotterTemplates templates) {
        this.fxQuoteServiceAsync = fxQuoteServiceAsync;
        this.tenorCell = tenorCell;
        this.storage = storage;
        this.messages = messages;
        this.clientBundle = clientBundle;
        this.props = props;
        this.templates = templates;
        initToolBar();
        initGrid();
        rootElement = uiBinder.createAndBindUi(this);
        initListeners();
	}

    @Override
    public void start(HasWidgets container) {
        final CurrencyPair defaultCurrencyPair = new CurrencyPair(Currency.AUD, Currency.USD);
        final int defaultAmount = 1000000;
        ccyComboBox.setValue(defaultCurrencyPair);
        amountNumberField.setValue(defaultAmount);
        getData(defaultCurrencyPair, defaultAmount);
        container.add(rootElement);
    }

    @Override
    public void stop() {}

    private void initToolBar() {
        ccyComboBox = new SimpleComboBox<CurrencyPair>(new LabelProvider<CurrencyPair>(){
            @Override
            public String getLabel(CurrencyPair item) {
                return item.getCcy1().getIso() + "/" + item.getCcy2().getIso();
            }
        });
        ccyComboBox.add(storage.getCurrencyPairs());
        ccyComboBox.setEmptyText(messages.ccyComboBoxText());
        amountNumberField = new NumberField<Integer>(new NumberPropertyEditor.IntegerPropertyEditor());
        amountNumberField.setFormat(Constants.CCY_AMOUNT_NUMBER_FORMAT);
    }

    private void initGrid() {
        columnConfigs = new ArrayList<ColumnConfig<MatrixRowModel, ?>>();
        ColumnConfig<MatrixRowModel, Tenor> tenorCol = new ColumnConfig<MatrixRowModel, Tenor>(props.tenor(), 35, "");
        tenorCol.setCell(tenorCell);
        columnConfigs.add(tenorCol);
        final PriceCell priceCell = new PriceCell();
        for (Tenor tenor : Tenor.values()) {
            ColumnConfig<MatrixRowModel, MatrixRowModelCellValue> columnConfig =
                    new ColumnConfig<MatrixRowModel, MatrixRowModelCellValue>(new MatrixRowValueProvider(tenor)
                            , Constants.MATRIX_BLOTTER_COLUMN_WIDTH, tenor.getName());
            columnConfig.setCell(priceCell);
            columnConfig.setColumnTextClassName(clientBundle.css().matrixBlotterCellInner());
            columnConfigs.add(columnConfig);
        }
        final ColumnModel<MatrixRowModel> columnModel = new ColumnModel<MatrixRowModel>(columnConfigs);
        store = new ListStore<MatrixRowModel>(props.key());
        matrixGrid = new Grid<MatrixRowModel>(store, columnModel);
        matrixGrid.setBorders(false);
        matrixGrid.addStyleName(clientBundle.css().matrixBlotterGrid());
    }

    private void initListeners() {
        recalculateButton.addHandler(new ClickHandler(){
            @Override
            public void onClick(ClickEvent event) {
                enableControlElements(false);
                getData(ccyComboBox.getValue(), amountNumberField.getValue());
            }
        }, ClickEvent.getType());
    }

    private void getData(CurrencyPair defaultCurrencyPair, int defaultAmount) {
        fxQuoteServiceAsync.getSwapMatrix(defaultCurrencyPair, defaultAmount
                , new GwtLogAsyncCallback<ArrayList<MatrixRowModel>>() {
            @Override
            public void onSuccess(ArrayList<MatrixRowModel> result) {
                store.clear();
                store.addAll(result);
                matrixGrid.getView().refresh(false);
                enableControlElements(true);
            }
        });
    }

    private void enableControlElements(boolean enabled) {
        ccyComboBox.setEnabled(enabled);
        amountNumberField.setEnabled(enabled);
        recalculateButton.setEnabled(enabled);
    }

    private class PriceCell extends AbstractCell<MatrixRowModelCellValue> {
		@Override
		public void render(com.google.gwt.cell.client.Cell.Context context, MatrixRowModelCellValue value, SafeHtmlBuilder sb) {
			switch (value.getStatus()) {
				case BUY:
					sb.append(templates.td(clientBundle.css().matrixBlotterCell()
							, clientBundle.css().matrixBlotterBuyCell()
							, Constants.QUOTE_SHORT_NUMBER_FORMAT.format(value.getPrice())));
					break;
				case SELL:
					sb.append(templates.td(clientBundle.css().matrixBlotterCell()
							, clientBundle.css().matrixBlotterSellCell()
							, Constants.QUOTE_SHORT_NUMBER_FORMAT.format(value.getPrice())));
					break;
				case NONE:
					break;
				case NO_DATA:
					break;
			}
		}		
	}

    private static MatrixBlotterViewUiBinder uiBinder = GWT.create(MatrixBlotterViewUiBinder.class);
    interface MatrixBlotterViewUiBinder extends UiBinder<VerticalLayoutContainer, MatrixBlotterView> {}
	
}
