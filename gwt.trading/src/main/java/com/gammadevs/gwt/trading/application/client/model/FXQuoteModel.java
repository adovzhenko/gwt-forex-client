/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.model;

import com.google.gwt.i18n.client.NumberFormat;

import com.gammadevs.gwt.trading.application.client.Constants;
import com.gammadevs.server.trading.client.model.FXQuote;

import java.math.BigDecimal;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class FXQuoteModel {

    private String buyPrice;
    private String buyPriceMajor;
    private String buyPriceMiddle;
    private String buyPriceMinor;

    private String sellPrice;
    private String sellPriceMajor;
    private String sellPriceMiddle;
    private String sellPriceMinor;

    public FXQuoteModel(FXQuote quote) {

        final BigDecimal fullBuyPrice = quote.getBuyPrice();
        final BigDecimal fullSellPrice = quote.getSellPrice();

        buyPrice = NumberFormat.getFormat(Constants.QUOTE_PATTERN).format(fullBuyPrice);
        buyPriceMajor = buyPrice.substring(0, 4);
        buyPriceMiddle = buyPrice.substring(4, 6);
        buyPriceMinor = buyPrice.substring(6);

        sellPrice = NumberFormat.getFormat(Constants.QUOTE_PATTERN).format(fullSellPrice);
        sellPriceMajor = sellPrice.substring(0, 4);
        sellPriceMiddle = sellPrice.substring(4, 6);
        sellPriceMinor = sellPrice.substring(6);
    }

    public String getBuyPrice() {
        return buyPrice;
    }

    public String getBuyPriceMajor() {
        return buyPriceMajor;
    }

    public String getBuyPriceMiddle() {
        return buyPriceMiddle;
    }

    public String getBuyPriceMinor() {
        return buyPriceMinor;
    }

    public String getSellPrice() {
        return sellPrice;
    }

    public String getSellPriceMajor() {
        return sellPriceMajor;
    }

    public String getSellPriceMiddle() {
        return sellPriceMiddle;
    }

    public String getSellPriceMinor() {
        return sellPriceMinor;
    }
}
