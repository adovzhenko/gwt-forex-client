/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.resources;

import com.google.gwt.resources.client.CssResource;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public interface MainCssResource extends CssResource {

    String buyBackgroundActive();
    String sellBackgroundActive();

    String buyBackground();
    String sellBackground();
    String rejectedBackground();
    String fxBackgroundInactive();
    String rowFailed();
    String rowNew();
    String dealPanelMargin();
    
    String matrixBlotterGrid();
    String matrixBlotterCellInner();
    String matrixBlotterCell();
    String matrixBlotterBuyCell();
    String matrixBlotterSellCell();
    
    String noTextSelection();
    
    String rateViewerRowPanel();
    String rateViewerDeleteRowButton();
    String rateViewerGreyButton();
    
    String quoteUp();
    String quoteDown();

    String dealPanelPortlet();

}
