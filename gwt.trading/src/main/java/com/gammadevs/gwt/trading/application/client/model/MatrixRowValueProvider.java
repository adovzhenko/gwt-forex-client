/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.model;

import com.sencha.gxt.core.client.ValueProvider;
import com.gammadevs.server.trading.client.model.MatrixRowModel;
import com.gammadevs.server.trading.client.model.MatrixRowModelCellValue;
import com.gammadevs.server.trading.client.model.Tenor;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class MatrixRowValueProvider implements ValueProvider<MatrixRowModel, MatrixRowModelCellValue> {

    private final Tenor tenor;

    public MatrixRowValueProvider(Tenor tenor) {
        this.tenor = tenor;
    }

    @Override
    public MatrixRowModelCellValue getValue(MatrixRowModel object) {
        return object.getPrices().get(tenor);
    }

    @Override
    public void setValue(MatrixRowModel object, MatrixRowModelCellValue value) {
        object.getPrices().put(tenor, value);
    }

    @Override
    public String getPath() {
        return "";
    }

}
