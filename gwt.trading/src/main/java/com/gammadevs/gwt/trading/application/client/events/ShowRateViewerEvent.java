/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class ShowRateViewerEvent extends GwtEvent<ShowRateViewerEvent.Handler> {
	
	private final boolean enabled;
	private final boolean firedManually;
	
	public ShowRateViewerEvent(boolean enabled, boolean firedManually) {
		this.enabled = enabled;
		this.firedManually = firedManually;
	}
 
    public boolean isEnabled() {
		return enabled;
	}

	public boolean isFiredManually() {
		return firedManually;
	}

	public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(Handler handler) {
        handler.handle(this);
    }
    
    public interface Handler extends EventHandler {
        void handle(ShowRateViewerEvent event);
    }
    public static Type<Handler> TYPE = new Type<Handler>();

}
