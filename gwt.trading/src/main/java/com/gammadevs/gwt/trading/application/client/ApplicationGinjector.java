/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client;

import com.gammadevs.server.trading.client.asyncservices.FXQuoteServiceAsync;
import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;
import com.google.web.bindery.event.shared.EventBus;
import com.gammadevs.gwt.trading.application.client.model.FXTradeResponseProperties;
import com.gammadevs.gwt.trading.application.client.model.cells.TenorCell;
import com.gammadevs.gwt.trading.application.client.providers.FXQuoteProvider;
import com.gammadevs.gwt.trading.application.client.resources.AppMessages;
import com.gammadevs.gwt.trading.application.client.resources.MainClientBundle;
import com.gammadevs.gwt.trading.application.client.storage.Storage;
import com.gammadevs.gwt.trading.application.client.widgets.DealPanelView;
import com.gammadevs.gwt.trading.application.client.widgets.FXTradeStatusView;
import com.gammadevs.gwt.trading.application.client.widgets.MainLayout;
import com.gammadevs.gwt.trading.application.client.widgets.TradeBlotterView;
import com.gammadevs.gwt.trading.application.client.widgets.ccychart.CcyChartView;
import com.gammadevs.gwt.trading.application.client.widgets.matrixviewer.MatrixBlotterView;
import com.gammadevs.gwt.trading.application.client.widgets.rateviewer.RateViewerView;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
@GinModules(ApplicationGinModule.class)
public interface ApplicationGinjector extends Ginjector {

    EventBus getEventBus();
    MainClientBundle getMainClientBundle();
    AppMessages getAppMessages();
    FXTradeResponseProperties getFXTradeResponseProperties();
    FXQuoteServiceAsync getFXQuoteServiceAsync();

    FXQuoteProvider getFXQuoteProvider();
    DealPanelInstanceManager getDealPanelInstanceManager();

    DealPanelView getDealPanelView();
    TradeBlotterView getTradeBlotterView();
    MainLayout getMainLayout();
    MatrixBlotterView getMatrixBlotterView();
    RateViewerView getRateViewerView();
    CcyChartView getCcyChartView();
    FXTradeStatusView getFXTradeStatusView();

    TenorCell getTenorCell();

    Storage getStorage();

}
