/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public interface MainClientBundle extends ClientBundle {

    @Source("com/gammadevs/gwt/trading/application/client/resources/main.css")
    MainCssResource css();
    
    @Source("images/delete18x18.png")
    ImageResource delete18x18();
    
    @Source("images/delete16x16.png")
    ImageResource delete16x16();
    
    @Source("images/delete16x16_highlighted.png")
    ImageResource delete16x16Highlighted();
    
    @Source("images/rightleft2green.png")
    ImageResource rightleft2green();
    
    @Source("images/rightleft2red.png")
    ImageResource rightleft2red();
    
    @Source("images/toolbar/coin_stack_silver_add24x24.png")
    ImageResource fxSpotOutrightIcon24();
    
    @Source("images/toolbar/coin_stack_silver_add16x16.png")
    ImageResource fxSpotOutrightIcon16();
    
    @Source("images/toolbar/chart16x16.png")
    ImageResource chart16();
    
    @Source("images/toolbar/blotter16x16.png")
    ImageResource blotter16();

    @Source("images/toolbar/rate_viewer16x16.png")
    ImageResource rateViewer16();
    
    @Source("images/matrixviewer/tenor-background.png")
    ImageResource tenorBackground();

}
