/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.model;

import java.math.BigDecimal;
import java.util.Date;

import com.gammadevs.server.trading.client.model.CcySide;
import com.gammadevs.server.trading.client.model.CurrencyPair;
import com.gammadevs.server.trading.client.model.FXTradeResponse;
import com.gammadevs.server.trading.client.model.FXTradeStatus;
import com.gammadevs.server.trading.client.model.Tenor;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class FXTradeResponseWrapper {

	private boolean newData;
	private FXTradeResponse response;
	
	public FXTradeResponseWrapper(boolean newData, FXTradeResponse response) {
		this.newData = newData;
		this.response = response;
	}

	public boolean isNewData() {
		return newData;
	}

	public void setNewData(boolean newData) {
		this.newData = newData;
	}
	
	public long getId() {
        return response.getId();
    }

    public FXTradeStatus getFxOrderStatus() {
        return response.getFxOrderStatus();
    }

    public CcySide getCcySide() {
        return response.getCcySide();
    }

    public CurrencyPair getCcyPair() {
        return response.getCcyPair();
    }
    
    public String getCcy1() {
        return response.getCcyPair().getCcy1().getIso();
    }
    
    public String getCcy2() {
        return response.getCcyPair().getCcy2().getIso();
    }

    public Integer getCcy1Amount() {
        return response.getCcy1Amount();
    }
    
    public Integer getCcy2Amount() {
        return response.getCcy2Amount();
    }

    public Tenor getTenor() {
        return response.getTenor();
    }

    public Date getDate() {
        return response.getDate();
    }

    public BigDecimal getPrice() {
        return response.getPrice();
    }
	
}
