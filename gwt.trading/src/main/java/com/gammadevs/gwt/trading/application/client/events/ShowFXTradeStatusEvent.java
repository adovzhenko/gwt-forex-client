/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.ui.Widget;

import com.gammadevs.server.trading.client.model.FXTradeResponse;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class ShowFXTradeStatusEvent extends GwtEvent<ShowFXTradeStatusEvent.Handler> {

    private final Widget parentWindow;
    private final FXTradeResponse fxOrderResponse;

    public ShowFXTradeStatusEvent(Widget parentWindow, FXTradeResponse fxOrderResponse) {
        this.parentWindow = parentWindow;
        this.fxOrderResponse = fxOrderResponse;
    }

    public Widget getParent() {
        return parentWindow;
    }

    public FXTradeResponse getFxOrderResponse() {
        return fxOrderResponse;
    }

    public interface Handler extends EventHandler {
        void handle(ShowFXTradeStatusEvent event);
    }
    public static Type<Handler> TYPE = new Type<Handler>();

    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(Handler handler) {
        handler.handle(this);
    }

}
