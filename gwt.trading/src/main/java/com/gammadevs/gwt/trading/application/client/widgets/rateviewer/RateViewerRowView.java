package com.gammadevs.gwt.trading.application.client.widgets.rateviewer;

import java.math.BigDecimal;

import com.gammadevs.gwt.common.client.mvp.IsMvpWidget;
import com.google.gwt.user.client.ui.HasWidgets;
import com.sencha.gxt.core.client.Style;
import com.sencha.gxt.fx.client.FxElement;
import com.sencha.gxt.fx.client.animation.Fx;

import com.gammadevs.gwt.trading.application.client.Constants;
import com.gammadevs.gwt.trading.application.client.events.DeleteRateViewerRowEvent;
import com.gammadevs.gwt.trading.application.client.events.ShowDealPanelEvent;
import com.gammadevs.gwt.trading.application.client.model.DealPanelModel;
import com.gammadevs.gwt.trading.application.client.model.FXQuoteModel;
import com.gammadevs.gwt.trading.application.client.model.RateViewerRowModel;
import com.gammadevs.gwt.trading.application.client.providers.FXQuoteProvider;
import com.gammadevs.gwt.trading.application.client.resources.MainClientBundle;
import com.gammadevs.server.trading.client.model.CcySide;
import com.gammadevs.server.trading.client.model.FXQuote;
import com.gammadevs.server.trading.client.model.FXQuoteRequest;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.web.bindery.event.shared.EventBus;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class RateViewerRowView implements IsMvpWidget, FXQuoteProvider.Listener {

    private final Widget rootElement;
	private final EventBus eventBus;
    private final FXQuoteProvider fxQuoteProvider;
    private final MainClientBundle clientBundle;
	private RateViewerRowModel model;
	private FXQuoteRequest request;

    private HasWidgets container;
	private FXQuote currentQuote;
	
	@UiField
    HTMLPanel contentPanel;
	@UiField
	DivElement deleteButton;
	@UiField
    DivElement ccyPair;
	@UiField
    DivElement tenor;
	@UiField
    DivElement amount;
	@UiField
    DivElement buyPriceMajor;
    @UiField
    DivElement buyPriceMiddle;
    @UiField
    DivElement buyPriceMinor;
    @UiField
    DivElement buyPriceTrend;
    @UiField
    DivElement sellPriceMajor;
    @UiField
    DivElement sellPriceMiddle;
    @UiField
    DivElement sellPriceMinor;
    @UiField
    DivElement sellPriceTrend;

    @Inject
	public RateViewerRowView(EventBus eventBus, FXQuoteProvider fxQuoteProvider, MainClientBundle clientBundle) {
		this.eventBus = eventBus;
		this.fxQuoteProvider = fxQuoteProvider;
        this.clientBundle = clientBundle;
		rootElement = uiBinder.createAndBindUi(this);
		initListeners();
	}
    
    public void setModel(RateViewerRowModel rateViewerRowModel) {
    	this.model = rateViewerRowModel;
    	this.request = new FXQuoteRequest(model.getCurrencyPair()
                , model.getTenor()
                , model.getAmountCcy()
                , CcySide.SELL
                , model.getTenorDate());
    	initModel();
    }
	
	private void initModel() {
		ccyPair.setInnerHTML(model.getCurrencyPair().getCcy1() + " / " + model.getCurrencyPair().getCcy2());
		tenor.setInnerHTML(model.getTenor().getName());
		amount.setInnerHTML(Constants.CCY_AMOUNT_NUMBER_FORMAT.format(model.getAmountCcy()));
	}
	
	private void initListeners() {
		rootElement.addDomHandler(new DoubleClickHandler(){
			@Override
			public void onDoubleClick(DoubleClickEvent event) {
				DealPanelModel dealPanelModel = new DealPanelModel(model.getCurrencyPair()
						, model.getTenor(), model.getAmountCcy(), null, model.getTenorDate());
		        eventBus.fireEvent(new ShowDealPanelEvent(dealPanelModel));
			}
		}, DoubleClickEvent.getType());
		DOM.setEventListener(deleteButton.<com.google.gwt.user.client.Element>cast(), new EventListener() {
            @Override
            public void onBrowserEvent(Event event) {
                if (event.getTypeInt() == Event.ONCLICK) {
                	eventBus.fireEvent(new DeleteRateViewerRowEvent(RateViewerRowView.this));
                }
            }
        });
        DOM.sinkEvents(deleteButton.<com.google.gwt.user.client.Element>cast(), Event.ONMOUSEOVER | Event.ONCLICK);
	}

    @Override
    public void onUpdate(FXQuote value) {
        final FXQuoteModel fxQuoteModel = new FXQuoteModel(value);
        buyPriceMajor.setInnerHTML(fxQuoteModel.getBuyPriceMajor());
        buyPriceMiddle.setInnerHTML(fxQuoteModel.getBuyPriceMiddle());
        buyPriceMinor.setInnerHTML(fxQuoteModel.getBuyPriceMinor());
        sellPriceMajor.setInnerHTML(fxQuoteModel.getSellPriceMajor());
        sellPriceMiddle.setInnerHTML(fxQuoteModel.getSellPriceMiddle());
        sellPriceMinor.setInnerHTML(fxQuoteModel.getSellPriceMinor());
        if (currentQuote != null) {
        	sellPriceTrend.setClassName(getQuoteTrendClass(currentQuote.getSellPrice(), value.getSellPrice()));
        	buyPriceTrend.setClassName(getQuoteTrendClass(currentQuote.getBuyPrice(), value.getBuyPrice()));
        }
        currentQuote = value; 
    }
    
    private String getQuoteTrendClass(BigDecimal currentValue, BigDecimal newValue) {
    	int comparison = currentValue.compareTo(newValue);
    	if (comparison == 0) {
    		return "";
    	}
    	if (comparison > 0) {
    		return clientBundle.css().quoteDown();
    	}
    	return clientBundle.css().quoteUp();
    }

    @Override
    public void start(HasWidgets container) {
        this.container = container;
        container.add(rootElement);
        fxQuoteProvider.addListener(request, this);
    }

    @Override
    public void stop() {
        fxQuoteProvider.removeListener(request, this);
        rootElement.getElement().<FxElement>cast().slideOut(Style.Direction.UP
                , new Fx(Constants.RATE_VIEWER_ROW_HIDE_ANIMATION_TIME) {
            @Override
            public void onComplete() {
                container.remove(rootElement);
                super.onComplete();
            }
        });
    }

    public void setLayoutData(Object layoutData) {
        rootElement.setLayoutData(layoutData);
    }
	
	private static RateViewerRowViewUiBinder uiBinder = GWT.create(RateViewerRowViewUiBinder.class);
    interface RateViewerRowViewUiBinder extends UiBinder<Widget, RateViewerRowView> {}

}
