/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.model.cells;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.gammadevs.server.trading.client.model.Tenor;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class TenorCell extends AbstractCell<Tenor> {
    @Override
    public void render(Context context, Tenor value, SafeHtmlBuilder sb) {
        sb.appendHtmlConstant(value.getName());
    }
}
