/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class ShowTradeBlotterEvent extends GwtEvent<ShowTradeBlotterEvent.Handler> {
	
	private final boolean enabled;
	private final boolean firedManually;
	
	public ShowTradeBlotterEvent(boolean enabled, boolean firedManually) {
		this.enabled = enabled;
		this.firedManually = firedManually;
	}
 
    public boolean isEnabled() {
		return enabled;
	}

	public boolean isFiredManually() {
		return firedManually;
	}

	public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(Handler handler) {
        handler.handle(this);
    }
    
    public interface Handler extends EventHandler {
        void handle(ShowTradeBlotterEvent event);
    }
    public static Type<Handler> TYPE = new Type<Handler>();

}
