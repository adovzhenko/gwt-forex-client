/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.trading.application.client.model;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public final class DealPanelModelValidator {

    private DealPanelModelValidator() {}

    public static boolean isValid(DealPanelModel model) {
        return (model.getAmountCcy1() != null || model.getAmountCcy2() != null)
                && model.getCurrencyPair() != null
                && model.getDate() != null
                && model.getTenor() != null;
    }
}
