package com.gammadevs.gwt.common.client.mvp;

import com.google.gwt.user.client.ui.HasWidgets;

public interface IsMvpWidget {

    void start(final HasWidgets container);
    void stop();

}
