package com.gammadevs.gwt.common.client.utils;

import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.ToolButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public final class DialogUtils {
	
	private DialogUtils() {}
	
	public static void addCloseButton(final ContentPanel dialog, final Function function) {
		dialog.getHeader().addTool(new ToolButton(ToolButton.CLOSE, new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                function.execute();
            }
        }));
	}

}
