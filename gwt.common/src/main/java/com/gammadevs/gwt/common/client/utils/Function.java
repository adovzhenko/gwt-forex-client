package com.gammadevs.gwt.common.client.utils;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public abstract class Function {

	public abstract void execute();
	
}
