package com.gammadevs.gwt.common.client.callback;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public abstract class GwtLogAsyncCallback<T> implements AsyncCallback<T> {

    @Override
    public void onFailure(Throwable caught) {
        GWT.log("Error on getting data from trading", caught);
    }
}
