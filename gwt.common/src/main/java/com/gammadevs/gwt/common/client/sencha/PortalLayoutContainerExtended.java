package com.gammadevs.gwt.common.client.sencha;

import com.google.gwt.uibinder.client.UiConstructor;
import com.sencha.gxt.widget.core.client.container.PortalLayoutContainer;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class PortalLayoutContainerExtended extends PortalLayoutContainer {
	
	@UiConstructor
	public PortalLayoutContainerExtended(int numColumns) {
		super(numColumns);
	}

	public PortalLayoutContainerExtended(PortalLayoutAppearance appearance, int numColumns) {
		super(appearance, numColumns);
	}
	
	public int getFirstFreeColumnIndex() {

		for (int i = 0; i < getColumnCount(); i++) {
			if (getWidget(i).getWidgetCount() == 0) {
				return i;
			}
		}

        return -1;
    }

}
