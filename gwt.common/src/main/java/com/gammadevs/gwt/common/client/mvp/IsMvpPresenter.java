package com.gammadevs.gwt.common.client.mvp;

public interface IsMvpPresenter<C> {
	
	void start(final C container);
    void stop();

}
