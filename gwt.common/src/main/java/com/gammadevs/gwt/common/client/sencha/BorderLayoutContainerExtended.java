package com.gammadevs.gwt.common.client.sencha;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.Style.LayoutRegion;
import com.sencha.gxt.widget.core.client.CollapsePanel;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.SplitBar;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;

/**
 * <code>BorderLayoutContainerExtended</code> extends
 * <code>BorderLayoutContainer</code> and adds possibility to expand and
 * collapse regions without firing <code>ExpandItemEvent</code> or
 * <code>CollapseItemEvent</code>
 * <p/>
 * Forex GWT portal POC.
 * 
 * @author Anton Dovzhenko
 */
public class BorderLayoutContainerExtended extends BorderLayoutContainer {

	/**
	 * Expands the panel in the given region. If the target widget is hidden no
	 * action will be performed.
	 * 
	 * @param region
	 *            <code>LayoutRegion</code> to be expanded
	 * @param fireEvent
	 *            equals <code>true</code> if ExpandItemEvent is fired after
	 *            expand, <code>false</code> otherwise.
	 */
	public void expand(LayoutRegion region, boolean fireEvent) {
		Widget w = getRegionWidget(region);
		if (w != null && w instanceof CollapsePanel) {
			BorderLayoutData data = getLayoutData(w);
			if (data.isHidden()) {
				return;
			}
			CollapsePanel collapse = (CollapsePanel) w;
			ContentPanel cp = (ContentPanel) collapse.getData("panel");
			onExpand(cp, fireEvent);
		}
	}

	/**
	 * Collapses the panel in the given region. If the target widget is hidden
	 * no action will be performed.
	 * 
	 * @param region
	 *            <code>LayoutRegion</code> to be collapsed
	 * @param fireEvent
	 *            equals <code>true</code> if ExpandItemEvent is fired after
	 *            collapse, <code>false</code> otherwise.
	 */
	public void collapse(LayoutRegion region, boolean fireEvent) {
		Widget w = getRegionWidget(region);
		if (w != null && w instanceof ContentPanel && !(w instanceof CollapsePanel)) {
			BorderLayoutData data = getLayoutData(w);
			if (data.isHidden()) {
				return;
			}
			onCollapse((ContentPanel) w, fireEvent);
		}
	}

	protected void onExpand(ContentPanel panel, boolean fireEvent) {
		if (fireEvent) {
			super.onExpand(panel);
		} else {
			CollapsePanel cp = panel.getData("collapse");
			LayoutRegion region = getRegion(cp);
			setRegionWidget(panel, region);
			setCollapsed(panel, false);
			BorderLayoutData data = (BorderLayoutData) panel.getLayoutData();
			data.setCollapsed(false);
			doLayout();
		}
	}

	protected void onCollapse(ContentPanel panel, boolean fireEvent) {
		if (fireEvent) {
			super.onCollapse(panel);
		} else {
			BorderLayoutData data = (BorderLayoutData) panel.getLayoutData();
			LayoutRegion region = getRegion(panel);
			CollapsePanel cp = (CollapsePanel) panel.getData("collapse");
			if (cp == null) {
				cp = createCollapsePanel(panel, data, region);
			}
			cp.clearSizeCache();
			setCollapsed(panel, true);
			setRegionWidget(cp, region);
			panel.clearSizeCache();
			doLayout();
			SplitBar bar = cp.getSplitBar();
			if (bar != null) {
				bar.sync();
			}
		}
	}

	@SuppressWarnings("incomplete-switch")
	private void setRegionWidget(IsWidget widget, LayoutRegion region) {
		switch (region) {
		case WEST:
			setWestWidget(widget);
			break;
		case EAST:
			setEastWidget(widget);
			break;
		case NORTH:
			setNorthWidget(widget);
			break;
		case SOUTH:
			setSouthWidget(widget);
			break;
		}
	}

	private BorderLayoutData getLayoutData(Widget w) {
		Object o = w.getLayoutData();
		return (BorderLayoutData) ((o instanceof BorderLayoutData) ? o : new BorderLayoutData(100));
	}

	private native void setCollapsed(ContentPanel panel, boolean collapse) /*-{
		panel.@com.sencha.gxt.widget.core.client.ContentPanel::collapsed = collapse;
	}-*/;

}
