/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client.widgets.admin.table;

import com.gammadevs.gwt.admin.client.Constants;
import com.gammadevs.gwt.common.client.mvp.IsMvpPresenter;
import com.gammadevs.server.admin.client.AdminEntityEnum;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.inject.Inject;

import java.util.*;

public class AdminTablePresenter implements IsMvpPresenter<HasWidgets> {

    public interface Display extends IsWidget {
        void setModel(List<String> columns);
        void setData(List<Map<String,String>> data);
    }

    private final Display display;

    private AdminEntityEnum entity;

    @Inject
    public AdminTablePresenter(Display display) {
        this.display = display;
    }

    @Override
    public void start(HasWidgets container) {
        container.add(display.asWidget());
    }

    @Override
    public void stop() {}

    public void setAdminEntity(AdminEntityEnum entity) {
        this.entity = entity;
        fetchData();
    }

    private void fetchData() {
        List<String> columns = new ArrayList<String>();
        columns.addAll(Arrays.asList(Constants.DEFAULT_KEY_NAME, "name_" + entity, "age", "gender", "education"));
        List<Map<String,String>> data = new ArrayList<Map<String,String>>();
        for (int i = 1; i < 20; i++) {
            Map<String,String> row = new HashMap<String, String>();
            row.put(Constants.DEFAULT_KEY_NAME, "" + i);
            row.put("name_" + entity, "name_" + i);
            row.put("age", "" + (i + 30));
            row.put("gender", i % 2 == 0 ? "M" : "F");
            row.put("education", i % 3 == 0 ? "school" : "university");
            data.add(row);
        }
        display.setModel(columns);
        display.setData(data);
    }
}
