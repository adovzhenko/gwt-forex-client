/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client;

public interface Constants {

    int DEFAULT_COLUMN_WIDTH = 50;
    String DEFAULT_KEY_NAME = "id";
    String ADMIN_MENU_BTN_KEY = "entity";
}
