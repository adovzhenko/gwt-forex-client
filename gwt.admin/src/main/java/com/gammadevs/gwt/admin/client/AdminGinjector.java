/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client;

import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;
import com.google.web.bindery.event.shared.EventBus;

import com.gammadevs.gwt.admin.client.widgets.MainLayoutPresenter;
import com.gammadevs.gwt.admin.client.widgets.admin.AdminToolbarPresenter;
import com.gammadevs.gwt.admin.client.widgets.admin.AdminLayoutPresenter;
import com.gammadevs.gwt.admin.client.widgets.admin.table.AdminTablePresenter;
import com.gammadevs.gwt.admin.client.widgets.db.DbLayoutPresenter;

@GinModules(AdminGinModule.class)
public interface AdminGinjector extends Ginjector {

	EventBus getEventBus();	
    MainLayoutPresenter getMainLayoutPresenter();
    MainLayoutPresenter.Display getMainLayoutView();
    AdminLayoutPresenter getAdminLayoutPresenter();
    AdminLayoutPresenter.Display getAdminLayoutView();
    DbLayoutPresenter getDbLayoutPresenter();
    DbLayoutPresenter.Display getDbLayoutView();
    AdminToolbarPresenter getAdminConsoleToolbarPresenter();
    AdminToolbarPresenter.Display getAdminConsoleToolbarView();
    AdminTablePresenter getAdminTablePresenter();
    AdminTablePresenter.Display getAdminTableView();

}
