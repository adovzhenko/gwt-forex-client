/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.web.bindery.event.shared.EventBus;

import com.gammadevs.gwt.admin.client.events.ChangeAdminEntityEvent;
import com.gammadevs.gwt.admin.client.events.ShowAdminConsoleEvent;
import com.gammadevs.gwt.admin.client.events.ShowDbConsoleEvent;
import com.gammadevs.gwt.admin.client.widgets.MainLayoutPresenter;
import com.gammadevs.gwt.admin.client.widgets.admin.AdminLayoutPresenter;
import com.gammadevs.gwt.admin.client.widgets.db.DbLayoutPresenter;

public class AdminController {

    private final AdminGinjector injector = GWT.create(AdminGinjector.class);

    private final EventBus eventBus;
    private MainLayoutPresenter mainLayoutPresenter;
    private AdminLayoutPresenter adminLayoutPresenter;
    private DbLayoutPresenter dbLayoutPresenter;

    AdminController() {
    	eventBus = injector.getEventBus();
    	mainLayoutPresenter = injector.getMainLayoutPresenter();
    	adminLayoutPresenter = injector.getAdminLayoutPresenter();
    	dbLayoutPresenter = injector.getDbLayoutPresenter();
    	initEventBusHandlers();
    }

    public void go(HasWidgets container) {
    	mainLayoutPresenter.start(container);
    }
    
    private void initEventBusHandlers() {
    	eventBus.addHandler(ShowAdminConsoleEvent.TYPE, new ShowAdminConsoleEvent.Handler() {
			@Override
			public void handle(ShowAdminConsoleEvent event) {
				dbLayoutPresenter.stop();
				adminLayoutPresenter.start(mainLayoutPresenter.getWorkspaceContainer());
			}
		});
    	eventBus.addHandler(ShowDbConsoleEvent.TYPE, new ShowDbConsoleEvent.Handler() {
			@Override
			public void handle(ShowDbConsoleEvent event) {
				adminLayoutPresenter.stop();
				dbLayoutPresenter.start(mainLayoutPresenter.getWorkspaceContainer());
			}
		});
        eventBus.addHandler(ChangeAdminEntityEvent.TYPE, new ChangeAdminEntityEvent.Handler() {
            @Override
            public void handle(ChangeAdminEntityEvent event) {
                GWT.log("Handling ChangeAdminEntityEvent(" + event.getEntity() + ") event.");
                adminLayoutPresenter.setAdminEntity(event.getEntity(), event.isChangeSelectedMenuItem());
            }
        });
    }
    
    EventBus getEventBus() {
    	return eventBus;
    }
    
}
