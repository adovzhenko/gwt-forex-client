/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client;

import com.gammadevs.gwt.admin.client.widgets.MainLayoutPresenter;
import com.gammadevs.gwt.admin.client.widgets.MainLayoutView;
import com.gammadevs.gwt.admin.client.widgets.admin.AdminToolbarPresenter;
import com.gammadevs.gwt.admin.client.widgets.admin.AdminToolbarView;
import com.gammadevs.gwt.admin.client.widgets.admin.AdminLayoutPresenter;
import com.gammadevs.gwt.admin.client.widgets.admin.AdminLayoutView;
import com.gammadevs.gwt.admin.client.widgets.admin.table.AdminTablePresenter;
import com.gammadevs.gwt.admin.client.widgets.admin.table.AdminTableView;
import com.gammadevs.gwt.admin.client.widgets.db.DbLayoutPresenter;
import com.gammadevs.gwt.admin.client.widgets.db.DbLayoutView;
import com.gammadevs.gwt.admin.client.widgets.db.DbToolbarPresenter;
import com.gammadevs.gwt.admin.client.widgets.db.DbToolbarView;

import com.google.gwt.inject.client.AbstractGinModule;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;

public class AdminGinModule extends AbstractGinModule {

    @Override
    protected void configure() {
    	bind(EventBus.class).to(SimpleEventBus.class).in(Singleton.class);
    	bind(MainLayoutPresenter.Display.class).to(MainLayoutView.class).in(Singleton.class);   	
    	bind(AdminLayoutPresenter.Display.class).to(AdminLayoutView.class).in(Singleton.class);   	
    	bind(DbLayoutPresenter.Display.class).to(DbLayoutView.class).in(Singleton.class);   	
    	bind(AdminToolbarPresenter.Display.class).to(AdminToolbarView.class).in(Singleton.class);   	
    	bind(DbToolbarPresenter.Display.class).to(DbToolbarView.class).in(Singleton.class);   	
    	bind(AdminTablePresenter.Display.class).to(AdminTableView.class).in(Singleton.class);
    }

}
