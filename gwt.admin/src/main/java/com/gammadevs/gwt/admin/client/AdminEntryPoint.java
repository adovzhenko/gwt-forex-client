/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client;

import com.gammadevs.gwt.admin.client.events.ChangeAdminEntityEvent;
import com.gammadevs.gwt.admin.client.events.ShowAdminConsoleEvent;

import com.gammadevs.server.admin.client.AdminEntityEnum;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.web.bindery.event.shared.EventBus;

public class AdminEntryPoint implements EntryPoint {

    @Override
    public void onModuleLoad() {
        final AdminController controller = new AdminController();
        controller.go(RootLayoutPanel.get());
        initDefaultWorkspace(controller.getEventBus());
    }
    
    private void initDefaultWorkspace(EventBus eventBus) {
    	eventBus.fireEvent(new ShowAdminConsoleEvent());
        eventBus.fireEvent(new ChangeAdminEntityEvent(AdminEntityEnum.ENTERPRISE, true));
    }
    
}
