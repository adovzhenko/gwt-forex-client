/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client.widgets.db;

import java.util.List;

import com.gammadevs.gwt.common.client.mvp.IsMvpPresenter;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.inject.Inject;
import com.sencha.gxt.widget.core.client.container.HasNorthWidget;

public class DbToolbarPresenter implements IsMvpPresenter<HasNorthWidget> {
	
	public interface Display extends IsWidget {
		List<? extends HasValue<Boolean>> getMenuBtns();
	}
	
	private final Display display;
	
	@Inject
	public DbToolbarPresenter(Display display) {
		this.display = display;
		initHandlers();
	}

	@Override
	public void start(HasNorthWidget container) {
		container.setNorthWidget(display);
	}

	@Override
	public void stop() {}
	
	protected void initHandlers() {
		final ValueChangeHandler<Boolean> handler = new ToggleBtnValueChangeHandler();
		for (HasValue<Boolean> toggleBtn : display.getMenuBtns()) {
			toggleBtn.addValueChangeHandler(handler);
		}
	}
	
	private static final class ToggleBtnValueChangeHandler implements ValueChangeHandler<Boolean> {
		@Override
		public void onValueChange(ValueChangeEvent<Boolean> event) {
			GWT.log(event.getSource() + "");
		}
	}

}
