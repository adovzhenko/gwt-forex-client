/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client.widgets;

import com.gammadevs.gwt.common.client.sencha.BorderLayoutContainerExtended;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.button.ToggleButton;
import com.sencha.gxt.widget.core.client.container.HasCenterWidget;
import com.sencha.gxt.widget.core.client.event.BeforeSelectEvent;
import com.sencha.gxt.widget.core.client.event.BeforeSelectEvent.BeforeSelectHandler;

public class MainLayoutView implements MainLayoutPresenter.Display {

    private final BorderLayoutContainerExtended rootElement;
    
    @UiField
    ToggleButton adminConsoleBtn;
    @UiField
    ToggleButton dbConsoleBtn;

    public MainLayoutView() {
        rootElement = ourUiBinder.createAndBindUi(this);
        adminConsoleBtn.addBeforeSelectHandler(new BeforeSelectHandler(){
			@Override
			public void onBeforeSelect(BeforeSelectEvent event) {
				event.setCancelled(adminConsoleBtn.getValue());
			}
		});
        dbConsoleBtn.addBeforeSelectHandler(new BeforeSelectHandler(){
			@Override
			public void onBeforeSelect(BeforeSelectEvent event) {
				event.setCancelled(dbConsoleBtn.getValue());
			}
		});
    }
    
    @Override
	public Widget asWidget() {
		return rootElement;
	}
    
    @Override
	public HasCenterWidget getWorkspaceContainer() {
		return rootElement;
	}

	@Override
	public HasValue<Boolean> getAdminConsoleBtn() {
		return adminConsoleBtn;
	}

	@Override
	public HasValue<Boolean> getDbConsoleBtn() {
		return dbConsoleBtn;
	}
	
	interface MainLayoutViewUiBinder extends UiBinder<BorderLayoutContainerExtended, MainLayoutView> {}
    private static MainLayoutViewUiBinder ourUiBinder = GWT.create(MainLayoutViewUiBinder.class);

    
}