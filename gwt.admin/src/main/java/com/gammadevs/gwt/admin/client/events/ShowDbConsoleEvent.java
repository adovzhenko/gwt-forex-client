/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class ShowDbConsoleEvent extends GwtEvent<ShowDbConsoleEvent.Handler> {

	public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(Handler handler) {
        handler.handle(this);
    }
    
    public interface Handler extends EventHandler {
        void handle(ShowDbConsoleEvent event);
    }
    public static Type<Handler> TYPE = new Type<Handler>();

}
