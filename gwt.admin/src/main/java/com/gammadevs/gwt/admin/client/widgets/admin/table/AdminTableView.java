/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client.widgets.admin.table;

import com.gammadevs.gwt.admin.client.Constants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.gammadevs.gwt.admin.client.model.GenericMapValueProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AdminTableView implements AdminTablePresenter.Display {

    private final SimpleContainer rootElement;
    private Grid<Map<String,String>> grid;

    public AdminTableView() {
        rootElement = ourUiBinder.createAndBindUi(this);
    }

    @Override
    public void setModel(List<String> columns) {
        List<ColumnConfig<Map<String,String>,?>> columnConfigs = new ArrayList<ColumnConfig<Map<String,String>,?>>();
        for (String column : columns) {
            ColumnConfig<Map<String,String>,String> columnConfig =
                    new ColumnConfig<Map<String,String>, String>(new GenericMapValueProvider(column), Constants.DEFAULT_COLUMN_WIDTH, column);
            columnConfigs.add(columnConfig);
        }
        final ColumnModel<Map<String,String>> columnModel = new ColumnModel<Map<String,String>>(columnConfigs);
        final ListStore<Map<String,String>> store = new ListStore<Map<String,String>>(new ModelKeyProvider<Map<String,String>>(){
            @Override
            public String getKey(Map<String, String> item) {
                return item.get(Constants.DEFAULT_KEY_NAME);
            }
        });
        grid = new Grid<Map<String,String>>(store, columnModel);
        grid.getView().setAutoFill(true);
        rootElement.setWidget(grid);
        rootElement.forceLayout();
    }

    @Override
    public void setData(List<Map<String,String>> data) {
        grid.getStore().addAll(data);
    }

    @Override
    public Widget asWidget() {
        return rootElement;
    }

    interface AdminTableViewUiBinder extends UiBinder<SimpleContainer, AdminTableView> {}
    private static AdminTableViewUiBinder ourUiBinder = GWT.create(AdminTableViewUiBinder.class);
}