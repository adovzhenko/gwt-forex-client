/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client.widgets.db;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.button.ToggleButton;
import com.sencha.gxt.widget.core.client.event.BeforeSelectEvent;
import com.sencha.gxt.widget.core.client.event.BeforeSelectEvent.BeforeSelectHandler;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class DbToolbarView implements DbToolbarPresenter.Display {
	
	private final ToolBar rootPanel;
	private final List<ToggleButton> buttons = new ArrayList<ToggleButton>(12);
	
	private ToggleButton selected;
	
	@UiField ToggleButton groupBtn;
	@UiField ToggleButton accountBtn;
	@UiField ToggleButton etsBtn;
	@UiField ToggleButton userIntBtn;
	@UiField ToggleButton userExtBtn;
	@UiField ToggleButton userPlatfBtn;
	@UiField ToggleButton permissionsBtn;
	@UiField ToggleButton tiBtn;
	@UiField ToggleButton fwdCurvesBtn;
	@UiField ToggleButton tableGroupingBtn;
	@UiField ToggleButton commonBtn;
	@UiField ToggleButton fxBtn;

	public DbToolbarView() {
		rootPanel = uiBinder.createAndBindUi(this);
		selected = groupBtn;
		buttons.add(groupBtn);
		buttons.add(accountBtn);
		buttons.add(etsBtn);
		buttons.add(userIntBtn);
		buttons.add(userExtBtn);
		buttons.add(userPlatfBtn);
		buttons.add(permissionsBtn);
		buttons.add(tiBtn);
		buttons.add(fwdCurvesBtn);
		buttons.add(tableGroupingBtn);
		buttons.add(commonBtn);
		buttons.add(fxBtn);
		initHandlers();
	}
	
	@Override
	public Widget asWidget() {
		return rootPanel;
	}
	
	@Override
	public List<? extends HasValue<Boolean>> getMenuBtns() {
		return buttons;
	}
	
	private void initHandlers() {
		final BeforeSelectHandler handler = new ButtonBeforeSelectHandler();
		for (ToggleButton button : buttons) {
			button.addBeforeSelectHandler(handler);
		}
	}
	
	private final class ButtonBeforeSelectHandler implements BeforeSelectHandler {
		@Override
		public void onBeforeSelect(BeforeSelectEvent event) {
			boolean currentValue = ((ToggleButton) event.getSource()).getValue();
			event.setCancelled(currentValue);
			if (!currentValue) {
				selected.setValue(false, false);
				selected = ((ToggleButton) event.getSource());
			}
		}
	}
	
	private static DbConsoleToolbarViewUiBinder uiBinder = GWT.create(DbConsoleToolbarViewUiBinder.class);
	interface DbConsoleToolbarViewUiBinder extends UiBinder<ToolBar, DbToolbarView> {}

}
