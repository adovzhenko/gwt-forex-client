/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client.widgets;

import com.gammadevs.gwt.common.client.mvp.IsMvpPresenter;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.sencha.gxt.widget.core.client.container.HasCenterWidget;

import com.gammadevs.gwt.admin.client.events.ShowAdminConsoleEvent;
import com.gammadevs.gwt.admin.client.events.ShowDbConsoleEvent;

public class MainLayoutPresenter implements IsMvpPresenter<HasWidgets> {
	
	public interface Display extends IsWidget {
		HasCenterWidget getWorkspaceContainer();
		HasValue<Boolean> getAdminConsoleBtn();
		HasValue<Boolean> getDbConsoleBtn();
	}
	
	private final Display display;
	private final EventBus eventBus;
	
	private HasWidgets container;
	
	@Inject
	public MainLayoutPresenter(Display display, EventBus eventBus) {
		this.display = display;
		this.eventBus = eventBus;
		initHandlers();
	}

	@Override
	public void start(HasWidgets container) {
		this.container = container;
		container.add(display.asWidget());
	}

	@Override
	public void stop() {
		container.remove(display.asWidget());
        container = null;
	}
	
	public HasCenterWidget getWorkspaceContainer() {
		return display.getWorkspaceContainer();
	}
	
	protected void initHandlers() {
		display.getAdminConsoleBtn().addValueChangeHandler(new ValueChangeHandler<Boolean>(){
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				eventBus.fireEvent(new ShowAdminConsoleEvent());
				display.getDbConsoleBtn().setValue(false, false);
			}
		});
		display.getDbConsoleBtn().addValueChangeHandler(new ValueChangeHandler<Boolean>(){
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				eventBus.fireEvent(new ShowDbConsoleEvent());
				display.getAdminConsoleBtn().setValue(false, false);
			}
		});
	}

}
