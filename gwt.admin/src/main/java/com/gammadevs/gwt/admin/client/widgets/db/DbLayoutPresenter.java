/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client.widgets.db;

import com.gammadevs.gwt.common.client.mvp.IsMvpPresenter;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.inject.Inject;
import com.sencha.gxt.widget.core.client.container.HasCenterWidget;
import com.sencha.gxt.widget.core.client.container.HasLayout;
import com.sencha.gxt.widget.core.client.container.HasNorthWidget;

public class DbLayoutPresenter implements IsMvpPresenter<HasCenterWidget> {
	
	public interface Display extends IsWidget {
		HasNorthWidget getToolbarContainer();
	}
	
	private final Display display;
	private final DbToolbarPresenter dbToolbarPresenter;
	
	@Inject
	public DbLayoutPresenter(Display display, DbToolbarPresenter dbToolbarPresenter) {
		this.display = display;
		this.dbToolbarPresenter = dbToolbarPresenter;
	}

	@Override
	public void start(HasCenterWidget container) {
		container.setCenterWidget(display);
		if (container instanceof HasLayout) {
			((HasLayout) container).forceLayout();
		}
		dbToolbarPresenter.start(display.getToolbarContainer());
	}

	@Override
	public void stop() {
		dbToolbarPresenter.stop();
		display.asWidget().removeFromParent();
	}

}
