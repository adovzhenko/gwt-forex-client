/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client.widgets.db;

import com.gammadevs.gwt.common.client.sencha.BorderLayoutContainerExtended;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.sencha.gxt.widget.core.client.container.HasNorthWidget;

public class DbLayoutView implements DbLayoutPresenter.Display {
	
	private final BorderLayoutContainerExtended rootPanel;
	
	@Inject
	public DbLayoutView() {
		rootPanel = uiBinder.createAndBindUi(this);
	}
	
	@Override
	public Widget asWidget() {
		return rootPanel;
	}
	
	@Override
	public HasNorthWidget getToolbarContainer() {
		return rootPanel;
	}
	
	private static DbLayoutViewUiBinder uiBinder = GWT.create(DbLayoutViewUiBinder.class);
	interface DbLayoutViewUiBinder extends UiBinder<BorderLayoutContainerExtended, DbLayoutView> {}

}
