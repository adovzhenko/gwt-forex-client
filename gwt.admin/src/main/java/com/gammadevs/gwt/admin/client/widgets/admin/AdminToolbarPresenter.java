/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client.widgets.admin;

import java.util.Map;

import com.gammadevs.gwt.common.client.mvp.IsMvpPresenter;
import com.gammadevs.server.admin.client.AdminEntityEnum;
import com.google.web.bindery.event.shared.EventBus;
import com.gammadevs.gwt.admin.client.events.ChangeAdminEntityEvent;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.inject.Inject;
import com.sencha.gxt.widget.core.client.container.HasNorthWidget;

public class AdminToolbarPresenter implements IsMvpPresenter<HasNorthWidget> {
	
	public interface Display extends IsWidget {
		Map<? extends HasValue<Boolean>, AdminEntityEnum> getMenuBtns();
        void setSelected(HasValue<Boolean> btn);
	}
	
	private final Display display;
    private final EventBus eventBus;
	
	@Inject
	public AdminToolbarPresenter(EventBus eventBus, Display display) {
        this.eventBus = eventBus;
		this.display = display;
		initHandlers();
	}

	@Override
	public void start(HasNorthWidget container) {
		container.setNorthWidget(display);
	}

	@Override
	public void stop() {}

    //TODO: refactor this, use bidirectional map (Guava)
    public void selectMenuItem(AdminEntityEnum entity) {
        for (Map.Entry<? extends HasValue<Boolean>, AdminEntityEnum> entry : display.getMenuBtns().entrySet()) {
            if (entry.getValue().equals(entity)) {
                entry.getKey().setValue(true, false);
                display.setSelected(entry.getKey());
            } else {
                entry.getKey().setValue(false, false);
            }
        }
    }
	
	protected void initHandlers() {
		final ValueChangeHandler<Boolean> handler = new ToggleBtnValueChangeHandler();
		for (HasValue<Boolean> toggleBtn : display.getMenuBtns().keySet()) {
			toggleBtn.addValueChangeHandler(handler);
		}
	}
	
	private final class ToggleBtnValueChangeHandler implements ValueChangeHandler<Boolean> {
		@Override
		public void onValueChange(ValueChangeEvent<Boolean> event) {
            AdminEntityEnum entity = display.getMenuBtns().get(event.getSource());
            eventBus.fireEvent(new ChangeAdminEntityEvent(entity, false));
		}
	}

}
