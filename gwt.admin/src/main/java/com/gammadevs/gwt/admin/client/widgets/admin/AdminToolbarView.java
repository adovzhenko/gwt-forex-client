/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client.widgets.admin;

import java.util.HashMap;
import java.util.Map;

import com.gammadevs.server.admin.client.AdminEntityEnum;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.sencha.gxt.widget.core.client.button.ToggleButton;
import com.sencha.gxt.widget.core.client.event.BeforeSelectEvent;
import com.sencha.gxt.widget.core.client.event.BeforeSelectEvent.BeforeSelectHandler;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class AdminToolbarView implements AdminToolbarPresenter.Display {
	
	private final ToolBar rootPanel;
	private final Map<ToggleButton, AdminEntityEnum> buttons = new HashMap<ToggleButton, AdminEntityEnum>(13);
	
	private HasValue<Boolean> selected;
	
	@UiField ToggleButton enterpriseBtn;
	@UiField ToggleButton groupBtn;
	@UiField ToggleButton userBtn;
	@UiField ToggleButton userRoleBtn;
	@UiField ToggleButton platformBtn;
	@UiField ToggleButton platformInstanceBtn;
	@UiField ToggleButton platformAccountBtn;
	@UiField ToggleButton pouBtn;
	@UiField ToggleButton deskBtn;
	@UiField ToggleButton clientBookingBtn;
	@UiField ToggleButton pricingBtn;
	@UiField ToggleButton sciBtn;
	@UiField ToggleButton stpBtn;

	@Inject
	public AdminToolbarView() {
		rootPanel = uiBinder.createAndBindUi(this);
		buttons.put(enterpriseBtn, AdminEntityEnum.ENTERPRISE);
		buttons.put(groupBtn, AdminEntityEnum.GROUP);
		buttons.put(userBtn, AdminEntityEnum.USER);
		buttons.put(userRoleBtn, AdminEntityEnum.USER_ROLE);
		buttons.put(platformBtn, AdminEntityEnum.PLATFORM);
		buttons.put(platformInstanceBtn, AdminEntityEnum.PLATFORM_INSTANCE);
		buttons.put(platformAccountBtn, AdminEntityEnum.PLATFORM_ACCOUNT);
		buttons.put(pouBtn, AdminEntityEnum.POU_CONFIGURATION);
		buttons.put(deskBtn, AdminEntityEnum.DESK_MAPPING);
		buttons.put(clientBookingBtn, AdminEntityEnum.CLIENT_BOOKING_LOCATION);
		buttons.put(pricingBtn, AdminEntityEnum.PRICING_PROFILE);
		buttons.put(sciBtn, AdminEntityEnum.SCI_MAINTENANCE);
		buttons.put(stpBtn, AdminEntityEnum.CLIENT_STP);
		initHandlers();
	}
	
	@Override
	public Widget asWidget() {
		return rootPanel;
	}
	
	@Override
	public Map<? extends HasValue<Boolean>, AdminEntityEnum> getMenuBtns() {
		return buttons;
	}

    @Override
    public void setSelected(HasValue<Boolean> btn) {
        selected = btn;
    }

    private void initHandlers() {
		final BeforeSelectHandler handler = new ButtonBeforeSelectHandler();
		for (ToggleButton button : buttons.keySet()) {
			button.addBeforeSelectHandler(handler);
		}
	}
	
	private final class ButtonBeforeSelectHandler implements BeforeSelectHandler {
		@Override
		public void onBeforeSelect(BeforeSelectEvent event) {
			boolean currentValue = ((ToggleButton) event.getSource()).getValue();
			event.setCancelled(currentValue);
			if (!currentValue) {
				selected.setValue(false, false);
				selected = ((ToggleButton) event.getSource());
			}
		}
	}
	
	private static AdminConsoleToolbarViewUiBinder uiBinder = GWT.create(AdminConsoleToolbarViewUiBinder.class);
	interface AdminConsoleToolbarViewUiBinder extends UiBinder<ToolBar, AdminToolbarView> {}

}
