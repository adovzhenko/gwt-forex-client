/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client.model;

import com.sencha.gxt.core.client.ValueProvider;

import java.util.Map;

public class GenericMapValueProvider implements ValueProvider<Map<String, String>, String> {

    private final String key;

    public GenericMapValueProvider(String key) {
        this.key = key;
    }

    @Override
    public String getValue(Map<String, String> object) {
        return object.get(key);
    }

    @Override
    public void setValue(Map<String, String> object, String value) {
        object.put(key, value);
    }

    @Override
    public String getPath() {
        return key;
    }

}
