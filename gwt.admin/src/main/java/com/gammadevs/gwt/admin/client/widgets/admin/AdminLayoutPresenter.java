/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client.widgets.admin;

import com.gammadevs.gwt.common.client.mvp.IsMvpPresenter;
import com.gammadevs.server.admin.client.AdminEntityEnum;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.inject.Inject;
import com.sencha.gxt.widget.core.client.container.HasCenterWidget;
import com.sencha.gxt.widget.core.client.container.HasLayout;
import com.sencha.gxt.widget.core.client.container.HasNorthWidget;

import com.gammadevs.gwt.admin.client.widgets.admin.table.AdminTablePresenter;

public class AdminLayoutPresenter implements IsMvpPresenter<HasCenterWidget> {
	
	public interface Display extends IsWidget {
		HasNorthWidget getToolbarContainer();
        HasWidgets getTableContainer();
	}
	
	private final Display display;
	private final AdminToolbarPresenter adminToolbarPresenter;
	private final AdminTablePresenter adminTablePresenter;

	@Inject
	public AdminLayoutPresenter(Display display
			, AdminToolbarPresenter adminToolbarPresenter
            , AdminTablePresenter adminTablePresenter) {
		this.display = display;
		this.adminToolbarPresenter = adminToolbarPresenter;
		this.adminTablePresenter = adminTablePresenter;
	}

	@Override
	public void start(HasCenterWidget container) {
		container.setCenterWidget(display);
		if (container instanceof HasLayout) {
			((HasLayout) container).forceLayout();
		}
		adminToolbarPresenter.start(display.getToolbarContainer());
        adminTablePresenter.start(display.getTableContainer());
        adminTablePresenter.setAdminEntity(AdminEntityEnum.ENTERPRISE);
	}

	@Override
	public void stop() {
		adminToolbarPresenter.stop();
		display.asWidget().removeFromParent();
	}

    public void setAdminEntity(AdminEntityEnum entity) {
        setAdminEntity(entity, false);
    }

    public void setAdminEntity(AdminEntityEnum entity, boolean changeSelectedMenuItem) {
        adminTablePresenter.setAdminEntity(entity);
        if (changeSelectedMenuItem) {
            adminToolbarPresenter.selectMenuItem(entity);
        }
    }

}
