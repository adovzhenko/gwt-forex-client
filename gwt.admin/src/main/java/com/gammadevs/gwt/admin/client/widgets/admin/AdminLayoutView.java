/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client.widgets.admin;

import com.gammadevs.gwt.common.client.sencha.BorderLayoutContainerExtended;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HasWidgets;
import com.sencha.gxt.widget.core.client.ContentPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.container.HasNorthWidget;

public class AdminLayoutView implements AdminLayoutPresenter.Display {
	
	private final BorderLayoutContainerExtended rootPanel;

    @UiField ContentPanel tableContainer;

	public AdminLayoutView() {
		rootPanel = uiBinder.createAndBindUi(this);
	}
	
	@Override
	public Widget asWidget() {
		return rootPanel;
	}
	
	@Override
	public HasNorthWidget getToolbarContainer() {
		return rootPanel;
	}

    @Override
    public HasWidgets getTableContainer() {
        return tableContainer;
    }

    private static AdminLayoutViewUiBinder uiBinder = GWT.create(AdminLayoutViewUiBinder.class);
	interface AdminLayoutViewUiBinder extends UiBinder<BorderLayoutContainerExtended, AdminLayoutView> {}

}
