/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.gwt.admin.client.events;

import com.gammadevs.server.admin.client.AdminEntityEnum;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Forex GWT portal POC.
 * @author Anton Dovzhenko
 */
public class ChangeAdminEntityEvent extends GwtEvent<ChangeAdminEntityEvent.Handler> {

    private final AdminEntityEnum entity;
    private final boolean changeSelectedMenuItem;

    public ChangeAdminEntityEvent(AdminEntityEnum entity, boolean changeSelectedMenuItem) {
        this.entity = entity;
        this.changeSelectedMenuItem = changeSelectedMenuItem;
    }

    public AdminEntityEnum getEntity() {
        return entity;
    }

    public boolean isChangeSelectedMenuItem() {
        return changeSelectedMenuItem;
    }

    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(Handler handler) {
        handler.handle(this);
    }
    
    public interface Handler extends EventHandler {
        void handle(ChangeAdminEntityEvent event);
    }
    public static Type<Handler> TYPE = new Type<Handler>();

}
