/***********************************************************************************
 *
 * Copyright 2014 by Gammadevs. Anton Dovzhenko.
 *
 * All rights reserved.
 *
 ************************************************************************************/
package com.gammadevs.server.admin.client;

public enum AdminEntityEnum {
	ENTERPRISE
	, GROUP
	, USER
	, USER_ROLE
	, PLATFORM
	, PLATFORM_INSTANCE
	, PLATFORM_ACCOUNT
	, POU_CONFIGURATION
	, DESK_MAPPING
	, CLIENT_BOOKING_LOCATION
	, PRICING_PROFILE
	, SCI_MAINTENANCE
	, CLIENT_STP
}
